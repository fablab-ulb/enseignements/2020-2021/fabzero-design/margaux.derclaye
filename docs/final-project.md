# FINAL PROJECT

PDF A4 : Pour télécharger la fiche technique, cliquez [ici](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/margaux.derclaye/-/raw/master/docs/images/Fiche_A4.pdf?inline=false). 

## OVERVIEW OF THE FINAL PROJECT

![](images/p2-min.jpg)
![](images/p4-min.jpg)
![](images/v_bleu-min.jpg)

**Description** 

Les vases « Tre Versatili » sont des variantes du Tabouret Pilastro appartenant à la collection  : «Kartell goes Sottsass - A Tribute to Memphis», lancée en 2015 en hommage au maître du design Ettore Sottsass. Les vases s’y réfèrent et rendent un deuxième hommage au « tabouret pilastro » par ses formes géométriques simples, claires et totémiques, empilées tel un jeu de construction et par ses couleurs vives, fortement influencées par le Pop Art. Les objets s’en différencient sous l’inspiration du studio « uau project » par sa modularité, sa grande variété de couleurs, tant vives que douces et par sa volonté de mettre l’utilisateur en premier plan. Ce desgin permet aux utilisateurs de choisir son modèle/ses modèles, ses couleurs et la taille de ses  pièces dans le but de choisir sa propre esthétique en choissisant entièrement son vase/ses vases sur base des gabarits proposés. L’objet de design « Tre Versatili » est une trilogie de vases modulables aux formes géométriques simples et totémiques, dotée d’assemblages simples, ludiques et infinis aux couleurs originales. Ces vases sont synonyme d’hommage à Ettore Sottsass et d’ambiance bariolée.

**Détails techniques** 

Pour plus d'informations sur les détails techniques de réalisation, sur les couleurs, sur les différents gabarits proposés et pour plus de photographies de différentes morphologiqes de vases : je vous invite à cliquer [ici](#the-project).

**Fichiers STL**

1. Pièce capuchon : 
<iframe src="https://hotmail183898.autodesk360.com/shares/public/SH919a0QTf3c32634dcf6a92c4a7b9f4609b?mode=embed" width="640" height="480" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"  frameborder="0"></iframe>

2. Pièce intermédiaire : 
<iframe src="https://hotmail183898.autodesk360.com/shares/public/SH919a0QTf3c32634dcf3730e15d0f17f14a?mode=embed" width="640" height="480" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"  frameborder="0"></iframe>

3. Pièce dédoublement tube : 
<iframe src="https://hotmail183898.autodesk360.com/shares/public/SH919a0QTf3c32634dcf45435d402943b53f?mode=embed" width="640" height="480" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"  frameborder="0"></iframe>

4. Pièce au dessus du dédoublement : 
<iframe src="https://hotmail183898.autodesk360.com/shares/public/SH919a0QTf3c32634dcfec1b05837b038213?mode=embed" width="640" height="480" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"  frameborder="0"></iframe>

5. Pièce en dessous du dédoublement : 
<iframe src="https://hotmail183898.autodesk360.com/shares/public/SH919a0QTf3c32634dcf495ef5c35d4e8f8b?mode=embed" width="640" height="480" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"  frameborder="0"></iframe>

6. Base grand vase : 
<iframe src="https://hotmail183898.autodesk360.com/shares/public/SH919a0QTf3c32634dcf5cbe837276ebb432?mode=embed" width="640" height="480" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"  frameborder="0"></iframe>

7. Base vase intermédiaire : 
<iframe src="https://hotmail183898.autodesk360.com/shares/public/SH919a0QTf3c32634dcf9521cc1310e5d359?mode=embed" width="640" height="480" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"  frameborder="0"></iframe> 

8. Base soliflore :
<iframe src="https://hotmail183898.autodesk360.com/shares/public/SH919a0QTf3c32634dcf821d736ee4382c7d?mode=embed" width="640" height="480" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"  frameborder="0"></iframe>

**Catalogue**

Pour télécharger le catalogue des vases cliquez [ici](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/margaux.derclaye/-/raw/master/docs/images/Catalogue_Design.pdf?inline=false). 

# ORIGINAL OBJECT

Afin de vous en apprendre plus sur l'objet en question, je vous remets ci-dessous, les explications que j'ai fournies dans la partie [Index](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/margaux.derclaye/-/blob/master/docs/index.md) de mon Git. 

- **Tabouret Pilastro, by Ettore Sottsass - Kartell**

> Informations générales
```
Collection : Kartell - 2015
Designer : Ettore Sottsass - 2005
Couleur : lilas
Matière : Technopolymère thermoplastique teinté dans la masse
Dimensions : Hauteur : 46 cm x Ø : 35 cm
Poids : 3,5 kg
```

> Description de l'objet  

En 2015, la collection « Kartell goes Sottsass - A Tribute to Memphis » lance trois pièces inédites conçues par le maître du Design Ettore Sottsass afin de lui rendre hommage. Dans cette collection se trouvent les tabourets Pilastro et Colonna ainsi que le vase Calice. L’objet que je vais analyser tout au long du quadrimestre est le tabouret Pilastro. Au musée, le tabouret est exposé en lilas mais il existe en quatre autres couleurs : rouge, violet, vert et noir. Il s’inspire de l’architecture antique grâce à ses lignes réalisées en technopolymère thermoplastique teintées dans la masse peint. Cet objet superpose des formes géométriques simples, joue avec la couleur et a une silhouette totémique. Ce tabouret se distingue des autres par sa polyvalence : il peut soit être utilisé comme un tabouret soit comme table d’appoint.

![](images/dimensions.jpg)

© M.D   

# RESEARCH


## EXERCICE N°1 : how to rethink an object? 

Avant de commencer le projet final, il est important de se positonner par rapport à celui-ci et la manière dont nous allons le repenser. Pour y parvenir, Victor nous à compilé **5 mots** dont les définitions de chacun proviennent de **4 dictionnaires : Le Wiktionnaire, Le petit Robert, Le petit Larousse et Le Littré**. Ces 4 dictionnaires sont d'approches mais également d'époques différentes. Certains d'entre eux sont plus contemporains et évolutifs tandis que d'autres sont plus classiques et littéraires. 

- Influence
- **Inspiration**
- Référence
- Hommage
- Extension

## My word and its definitions 

Après avoir lu et comparé les différentes définitions données, un mot m'a particulèrement touché : **INSPIRATION**. Ce mot correspond le plus à l'attitude que je veux adopter tout au long de ce travail de réflexion. Cependant, étant un terme vague, on a souvent des difficultés à comprendre son origine. Peu d'entres nous le connaissent correctement, moi y compris. C'est pour cette raison que j'ai décidé de dialoguer avec celui-ci. J'ai donc commencé par établir une liste de toutes les défintions détaillées qui m'ont le plus évoqués un sentiment :

- **Le Wiktionnaire** : dictionnaire contemporain et collaboratif 
```
• Acte de stimulation de l’intellect, des émotions et de la créativité à partir d’une influence.

• Je vagis doucement dans l’extase de mon inspiration et je perds presque conscience. 
— (Knut Hamsun, La Faim, traduction de Georges Sautreau, 1961, page 268).

• Idée créatrice, élan créateur d'origine mystérieuse. 
--> Je manque d'inspiration. L'inspiration ne vient pas. 
--> Céder à l’inspiration.
--> Cette faculté singulière, toujours dominante et jamais soumise, inégale, indisciplinable, 
impitoyable, venant à son heure et s’en allant comme elle était venue, ressemblait, à s’y méprendre,
à ce que les poëtes nomment l’inspiration et personnifient dans la Muse.
— (Eugène Fromentin, Dominique, L. Hachette et Cie, 1863, réédition Gründ, page 92).
--> Dans le secret de ton être peut se manifester l'inspiration. Ce mot, « inspiration », vient du latin in spiritu : cela signifie
que tu es alors immergé dans la profondeur de ton propre esprit. En cet instant privilégié s'entremêlent l'intuition, le sentiment,
l'émotion et la joie. Tu as alors rendez-vous avec ta créativité. 
— (Jean Proulx, Grandir en humanité, Fides, 2018, page 54).
```
- **Le petit Robert** : dictionnaire ancien sur l'étymologie des mots 
```
• Souffle créateur qui anime les artistes, les chercheurs. L'inspiration poétique. Attendre 
l'inspiration.

• Action d'inspirer, de conseiller qqch. à qqn ; résultat de cette action. ➙ influence, 
instigation.

• (ŒUVRE, ART) D'inspiration (+ ADJECTIF), qui s'inspire de (une œuvre du passé...). 
Une musique d'inspiration romantique.

• Idée, résolution spontanée, soudaine. Une heureuse inspiration.
```
- **Le petit Larousse** : dictionnaire français classique et très diffusé 
```
• Mouvement intérieur, impulsion qui porte à faire, à suggérer ou à conseiller quelque action : suivre son inspiration.

• Enthousiasme, souffle créateur qui anime l'écrivain, l'artiste, le chercheur : 
chercher l'inspiration.

• Influence exercée sur un auteur, sur une œuvre: une décoration d'inspiration crétoise. 
```
- **Le Littré** : dictionnaire littéraire de la fin du 19ème siècle
```
• L'enthousiasme qui entraîne les poètes, les musiciens, les peintres. 

• Action de conseiller quelqu'un, de lui suggérer quelque chose.
--> La chose inspirée. Je vous dois cette inspiration. Il n'écoute que les inspirations de sa 
fureur.
``` 
- Why this word?

Ce choix a été simple car selon moi, l'inspiration est une **source spontannée**, elle est dans la plupart des cas, dûe au **hasard**. Elle se manifeste de manière audacieuse à travers ce que l'on peut penser, ressentir ou imagner. On peut la décrire comme une **source fulgurante** qui répond à un élément qui n'avait jusqu'à cet instant pas de réponse ou encore comme une **illumination** qui abrite notre esprit et nos pensées. Ça nous frappe sans le vouloir! Quand cette inspiration arrive, il est **essentiel de l'écrire, de la dessiner, de l'esquisser ou de la représenter** d'une quelconque manière avant qu'elle disparaîsse de notre esprit. C'est pour cette raison que j'ai toujours un carnet de dessin sur moi, il me permet de ne jamais l'oublier. Il est important de toujours **écouter nos idées/nos inspirations** et d'aller au bout de celles-ci. L'inspiration est bien plus forte que notre intuition. Contrairement à l'intuition, l'inspiration nous mène directement sur la voie souhaitée, une fois qu'elle apparait, elle va droit au but. 

Pour ma part, la créativité va de paire avec l'inspiration, elle nourrit mes idées. Elles sont constamment présentes dans mon quotidien, elles vivent avec moi. **Tout peut être inspirant**, une référence, un texte, un dessin, une discussion,... Même un sujet banal peut nous inspirer, il faut juste être attentif. C'est ce que j'affectionne tout particulièrement avec l'inspiration. 

L'acte de s'inspirer, peu importe sa source, va servir à tout le monde même si ce n'est pas créatif. Tout au long du projet, le terme **inspiration va guider et nourrir mon esprit et ma créativité**. Il va me permettre d'ajouter des idées originiales ou de solutionner des problèmes lors de la création de mon objet en faisant des recherches, des croquis,... 

# THOUGHT PROCESS

- Comment vais-je procéder? 

Pour mener à bien ce projet final, je ne compte pas me restreindre à une idée bien précise mais je vais me laisser guider tout au long de ma conception par mes inspirations, mes recherches, mes références, mes idées et ma créativité. Lors de ce processus de pensée, je vais probablement passer par deux phases : 

- **Avoir un tas d'idées créatives**
- **Ne pas avoir d'idées, être en manque d'inspiration**

Parfois je vais être submerger d'idées, savoir commencer ou encore ne pas savoir par où commencer. Cependant, je vais surement être en **manque d'inspiration**, c'est un comble quand tout est source d'inspiration. Comment cela s'explique? Nous ne sommes tout simplement pas toujours inspiré par tout, à chaque instant de notre journée. C'est pour cela qu'il est important de s'y mettre directement lorsque nous sommes inspirés. C'est à cet instant que la recherche va entrer en jeu, c'est à cet instant qu'il faut **chercher vers quoi s'inspirer**. Il faut **prendre le temps** de créer et d'imaginer, tout n'arrive pas par magie. C'est la stratégie que j'ai décidé de suivre pendant les différentes phases de mon travail.

Comme le dit Albert Einstein : « _le secret de la créativité c’est de savoir comment cacher ses sources_ ». 

- Moodboard 

Afin de me projeter dans mon travail, j'ai décidé de créer un moodboard pour laisser libre court à mon imagination. Cette planche va me permettre d'avoir l'inspiration nécessaire, d'orienter mon travail ainsi que de le nourrir et de le clarifier. Cet espace de liberté m'a permit de regrouper à la même place toutes les références intéressantes et inspirantes que j'ai pu récolter lors de mes recherches. Il va me permettre également de communiquer avec vous ce que je perçois de mon objet. 

![](images/Moodboard.jpg)

# BEFORE STARTING

> Comment se réapprorier l'objet?

Avant de commencer la conceptualisation de l'objet, j'ai d'abord effectué différents tests (tabouret/étagère/vase) afin d'orienter ces idées et voir celle qui fonctionne le mieux et qui me plait le plus. Dans cette partie j'ai imaginé trois types d'objets : l'objet modulable (le plug in) réalisé au début du cours, le test n°1 qui ressemble beaucoup à l'objet d'origine mais en plus élancé et le test n°2 qui s'inspire de l'objet mais qui ressemble à une ruche. 

- L'objet original et le plug-in 

![](images/Prototype_plug_in-min.jpg)

Sur ces images, on peut voir le prototype d'origine en gris foncé et le prototype plug in en bleu clair. Ce dernier est un objet modulable, qui sert d'une part comme assise ou comme table d'appoint si on aligne tous les anneaux. D'autre part, on peut aussi l'utiliser comme une petite étagère à côté de son fauteuil en modulant les anneaux pour y mettre des livres, un verre,... 

Ce projet n'a pas été difficle à réaliser, si vous voulez voir comment celui-ci à été réaliser sur Fusion 360, vous pouvez consulter le [Module 2](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/margaux.derclaye/-/blob/master/docs/modules/module02.md) de mon Git. 

## Test 1

![](images/test_1_fusion-min.jpg)

Ce prototype à été imaginé comme un pot/grand vase pour permettre deux choses : que ce soit, un élément conservateuur pour des plantes à longues tiges et une petite étagère à l'entrée d'une maison, utilisé comme vide-poche. Cet objet reprend le même principe de formes géométriques simples que le tabouret d'origine mais le tube est plus long, plus fin et plus élancé.

Il a été réaliser de la même manière sur Fusion que le plug in mais avec des proportions différentes :

1. Pour dessiner le premier tube du bas, j'ai utilisé l'outil **Solide** puis appuyé sur le petit **+** vert pour dessiner sur l'esquisse un cercle grâce à l'outil **Cercle** d'un diamètre de 12.50mm et une hauteur de 5mm grâce à l'outil **Extrusion**. 

2. Pour dessiner le premier anneaux, j'ai refais exactement les mêmes manipulations mais avec un diamètre de 22mm et une hauteur de 3mm.

3. Pour obtenir la version finalement j'ai du refaire 4 fois la première manipulation et 4 fois la deuxième manipulation.

4. Une fois la forme obtenue, j'ai utilisé l'outil **Perçage** afin de pouvoir créer mon trou pour les plantes. Je l'ai réalisé avec un diamètre de 10mm et une profondeur de 115mm. 

5. Après avoir réalisé le trou, j'ai utilisé l'outil **Congé** sur le bords de tous les cercles pour leurs donner une forme ronde sur ses extémités. 

![](images/test_1_final-min.jpg)

## Test 2

![](images/test_2_fusion-min.jpg)

Ce vase orange est imaginé de la même manière que celui au dessus mais avec plus d'anneaux de différentes tailles et beaucoup plus fins afin de tenter d'apporter un certaine élégance au vase. C'est comme une petite ruche où les éléments peuvent venir s'imbriquer dedans. 

Il a été réaliser de la même manière sur Fusion que le test n°1 mais avec des proportions différentes et plus fines :

1. A l'inverse du test n°1, j'ai commencé par créer un anneaux sur le plateau à l'aide des mêmes outils qu'énnoncé juste au dessus mais avec un diamètre de 30mm et d'une hauteur de 1mm.

2. Pour le tube, j'ai également utilisé les mêmes outils que pour le tube du premier test mais avec un dimètre de 15mm et une hauteur de 5mm.

3. Tout comme l'autre objet, j'ai répété les manipulations pour les anneaux encore 9 mais avec 2mm de diamètre en plus à chaque fois jusqu'au 10ème anneau. 

4. Pour le tube, j'ai également répété les manipulations 9 fois. Cependant, il y a une particularité contrairement à l'autre, les espaces ne sont pas pareil partout. A partir du 3ème anneau, j'ai réalisé une hauteur plus grande de 3mm pour après recommencer pendant 4 anneaux une hauteur de 5mm. Après le 7ème anneau, j'ai de nouveau réaugmenté la hauteur de 3mm pour ensuite retourner à 5mm pour le reste du tube. 

![](images/Test_2_final-min.jpg)

> Réglages d'impressions

Pour ces deux impressions (la verte et la orange), j'ai effectué les mêmes réglages pour les deux prototype. Afin de pouvoir les imprimer, j'ai changé 7 réglages : 

1. Hauteur de couche : 0,2
2. Périmètres : 3
3. Couches solides : 3 et 3
4. Densité de remplissage : 10% 
5. Remplissage : rectiligne 
6. Boucles (jupe) : 3
7. Support : oui

![](images/réglages_test-min.jpg)

> Impressions en 3D

![](images/test_impressions-min.jpg)

Comme vous pouvez le voir sur ces différentes photos, ce type d'impresssion avec des ponts demande énormement de support. Non seulement ça prend beaucoup plus de temps mais c'est aussi un type d'impression qui produit énormément de déchets. De plus, une fois l'impression terminée, ce n'est pas pour autant terminé car il faut enlever tous les supports et ça prend également beaucoup de temps. C'est pour ces deux raisons que j'ai décidé de ne plus continuer sur cette voie. L'idée est de ne pas créer de support afin de pouvoir construire des pièces à la chaine sans devoir rien faire une fois l'impression terminée! L'idée principale est de créer des prototypes rapides.

--> **N.B** : Le prototype blanc est le premier prototype de la "ruche" mais j'ai dû le recommencer en orange car il y a eu un problème d'impression. 

# RESEARCH FOR THE PROJECT

## Analysis of the functioning of Ettore Sottsass

Ettore Sottsass est un artiste qui navigue entre trois mondes : **l'architecture**, le **design industriel** et le **design expérimental**. Selon lui, il n'y a pas de définition « figée » du design car son rôle et son action dans le temps sont en perpétuel évolution. Il est tout de même possible d'en définir des fondamentaux : le design relève de la conception graphique comme du projet d'architecture. Il dit lui-même : « toutes mes créations ressemblent à des petites architectures ». Il traite les petits objets design et la grande architecture de la même manière. Il explique également que le design industriel est soumis la plupart du temps à l'univers industriel et **doit tenir comptes de contraintes** comme _son cahier des charges, les coûts et à sa faisabilité_. D'autre part, il se tourne également vers le design expérimental, vers **l'exploration des techniques, des formes, des typologies et des usages** en dehors des contraintes. Ce changement d'attitude donne la priorité sur l'évolution du rôle et de la fonction de l'objet dans la société, encourageant les consommateurs à revoir leur comportement. Cette expérimentation se montre sous la _forme de prototype ou d'une série limitée_.

Ce designeur est un adepte du **fonctionnalisme**, c'est-à-dire qu'il définit un objet comme un principe selon lequelle la **forme n'est que l'expression de l'usage**. Nous pouvons principalement dire que ses productions sont d'une **grande variété** et qu'elles demeurent du domaine du design industriel ainsi que du domaine artisanal. Son travail provient d'un language formel élaboré avec grande cohérence. Ettore réalise des **formes simples, claires, totémiques et géométriques** sur base d'éléments graphiques élémentaires tels que les cercles, les carrés, les lignes et les cylindres. Il arrache ces formes géométriques aux mathématiques et à la rigueur intellectuelle pour les renvoyer aux révolutions cosmiques.

Dans ces productions, Sottsass utilise énormement la **couleur**. C'est selon lui le vocabulaire de la vie. Ces créations jouent avec les couleurs et peuvent se superposer afin de libérer des énergies positives, vitales. Grâce à ces juxtapositions de formes et de couleurs, il offre un catalogue d'assemblage infini à la recherche de la meilleure proposition. Au fil des années, il pratique de plus en plus le **mélange de pièces et de matières incomptables** qui au final créent une certaine énergie, électricité. Ces associations reviennent sur les traces de la mémoire des éléments architecturaux. 

![](images/Analyse_Ettore-min.jpg)

## Main inspiration  : UAU PROJECT

Ces idées me sont principalement venue du studio de design UAU PROJECT à Varsovie en Pologne. Il a été fondé en 2011 par Justyna Faldzinska, diplomée du studio d'affiches de la faculté des Arts Graphiques et Milosz Dabrowski, diplomé de la faculté du design industriel de l'Académie des Beaux-Arts de Varsovie. Leurs créations sont toutes déstinées à une **utilisation domestique** et imaginées afin d'être imprimées en **3D**. Le studio utilise principalement du **PLA**, fait à partir de bioplastiques végétaux et est totalement recyclable et compostable. Ils n'utilisent que des matériaux biodégradables ou hautement recyclables. Leurs objectif est de prouver que cette machine est la meilleure solution pour un design durable de qualité et accesible. 

Les designeurs utilisent une **fabrication additive** (ajout de couches succesives grâce à un ordintateur) avec une production presque sans déchet, c'est-à-dire, **sans aucun matériau de support** lors de l'impression 3D. Il ne fabrique pas des produits à la chaine et ne font ni de surstockage. Tous leurs produits sont fabriqués à la demande. Leur production est entièrement **modulable** avec une possibilité de **100 couleurs**. Cette modularité permet à chaque client de concevoir un objet avec les couleurs de son choix. Ils travaillent sur le principe de l'**autonomie complète à l'utilisateur**. 

![](images/UAU.jpg)

## Description of the main idea/project

L'objet imaginé est un **vase** née d'une variante entre le **tabouret Pilastro** d'Ettore Sottsass et les productions du studio **UAU PROJECT**. Ce vase se veut **modulable**, c'est-à-dire que chaque utilisateur est invité à créer son vase sur base des différentes pièces proposées. Il peut choisir soit la forme, soit la couleur ou encore la hauteur de la pièce en fonction du type de vases dont il a besoin. Ce vase a pour but d'offrir une grande variété au sein de son catalogue. Les formes de ce vase seront des formes géométriques simples qui pourront s'assembler l'une à l'autre. Une fois assemblées, l'objet aura une forme totémique pour rappeler l'objet d'origine. Dans ce projet, il sera important de faire attention à l'étanchéité. 

Afin de m'aider à imaginer ce vase, voici quelques points qui vont me permettre de décrire mes idées pendant tout le processus :

```
- Géométrie simple 
- Couleur
- Modulable 
- Industriel
- Mémoire architecturale
- Totémique
```

## Definition of a vase 

Afin de définir les **fonctions du vase**, je vais définir ce qu'est un vase et à quoi il sert à l'aide de plusieurs définitions de dictionnaires :
```
• Récipient le plus souvent utilisé pour garder des fleurs coupées dans de l'eau. 
(L'Internaute)

• Récipient, de matière, de grandeur et de forme variables : vase à boire, vase funéraire. 
Récipient utilisé comme élément décoratif. (Larousse)

• Récipient pour contenir un fluide ou une substance granuleuse. Récipient de forme allongée, 
posé debout, utilisé pour contenir un liquide, des cendres, ou pour décorer. Calice de certaines 
fleurs. (Wikitionnaire)

• Ornement de sculpture, isolé et creux, qui, posé sur un socle ou piédestal, sert pour décorer les bâtimens et les jardins.
(Encyclopédie)

• Récipient de grandeur et de forme variables servant à différents usages ou ayant valeur 
historique, artistique. Récipient destiné à contenir des liquides. 
(Trésor de la Langue Française informatisé)

• Vase (du latin vas), se dit, en général, d'un ustensile destiné à contenir des liquides ou 
autres objets, et spécialement d'un vaisseau de forme élégante, à lèvres évasées, monté sur un
piédouche, et orné plus ou moins richement d'oves, godrons, guirlandes et figures en bas-relief, 
avec des anses sculptées.

```
Grâce à ces différentes définitions, nous pouvons établir la définition générale suivante : **un vase est un récipient de taille et de forme varié utilisé pour différents usages : conserver des fleurs dans de l'eau, contenir des objets et utilisé comme élément décoratif**. Lors de la réalisation de ce vase, je vais me concentrer et mettre en avant les trois fonctions principales suivantes : une **fonction conservatrice** (étanchéité), une **fonction variable** (modularité) et une **fonction décorative** (esthétique/couleur). 

## Types of vases : form, size and color

Lorsqu'on achète des fleurs, on ne sait pas toujours quel vase il faut utiliser afin de mettre le bouquet de fleurs choisi en valeur. Quelle couleur, forme et taille choisir? Pour commencer, il est important de prendre en compte la **taille/grosseur du bouquet** ainsi que la **règle d'or : le vase doit représenter un tier de l'ensemble et les fleurs les deux tiers**, c'est-à-dire que le bouquet de fleur doit mesurer 2x la taille du vase.

- Un soliflore (1)

Le soliflore est un vase qui, comme son nom l'indique, permet de mettre en avant **une, deux ou trois fleurs**. Pour ce vase, il est important de favoriser une forme étroite sur le dessus mais le dessous n'a pas d'importance : il peut être soit étroit soit large, soit rond soit carré. Il est d'ailleurs conseillé d'adapter la taille de la tige au vase. C'est un vase idéal lorsque le bouquet à perdu son éclat et qu'il reste quelques survivantes. 

- Un vase conique (2)

Le vase conique est un vase où sa hauteur est plus large que sa base, ce qui permet de donner plus de liberté aux fleurs. C'est le vase idéal pour un **bouquet de fleurs sauvages ou romantiques**. Il peut également convenir aux grands bouquets de fleurs colorés afin qu'elles aient toute la place nécessaire pour s'enrichir. 

- Un vase boule (3)

Le vase boule est un vase sphérique et particulièrement design. Il se compose d'une ouverture de taille moyenne et d'un intérieur beaucoup plus large. Cette forme petmet aux tiges des fleurs de se positionner comme elles le souhaitent de manière esthétique. Il conviendrait parfaitement aux lilas.

- Un vase cylindrique (4)

Aussi appelé vase tube, le vase cylindrique est un vase rectiligne qui est aussi large en bas qu'en haut et idéal pour les grands et hauts bouquets de fleurs. On choisira alors pour ce vase les **fleurs qui ont des longues et rigides tiges** (exemple : tulipes) pour que le vase se maintienne correctement et ainsi obtenir une forme harmonieuse. 

- Un vase transparent (5)

Pour un vase transparent, il est seulement important d'y mettre des **longues tiges**. En ce qui concerne la forme, on peut choisir n'importe quelle forme.

- Quelle couleur choisir pour un vase? 

Rien de plus facile, tout est possible! Si l'on choisi une couleur que l'on apprécie, elle se fondra forcèment dans la décoration de la pièce.

![](images/vase_ok_type-min.jpg)

## Quantity of water

Pour commencer, il faut savoir qu'avant de mettre de l'eau dans un vase, il est impératif que le vase soit **parfaitement propre**. Pourquoi? Si il n'est pas propre l'eau va s'imprégner des éventuelles germes présentent dans le fond du vase et ne pas bien conserver les fleurs. Il est généralement conseillé de **remplir son vase généreusement** d'eau fraîche à une **température ambiante**, ni trop froide ni trop chaude. Par exemple, pour les roses, il est conseillé d'immerger les fleurs à 2/3 de la hauteur des tiges pour qu'elles se conversent plus longtemps. 

Cependant, certaines fleurs comme les tupiles n'apprécient pas que ses tiges soit immergiées, il est alors conseillé de mettre un petit peu d'eau (plus ou moins 5cm) au fond du vase et vérifier que le niveau soit toujours le même. Il est toujours important de vérifier quelle quantité d'eau il faut mettre en fonction des types de fleurs car elles n'ont **pas toutes les mêmes besoins**. 

**Conseil** : si les fleurs commencent à perdre de leurs éclats, il est également conseillé de changer l'eau tous les deux jours et de recouper les tiges.

## 3D printers and its limits 

Comme je souhaite réaliser ce vase à l'aide des imprimantes 3D, je dois tout savoir sur elles! Pour ce faire, je me suis renseigné sur ses différentes contraintes et limites :

- La taille de l'imprimante 

Premièrement, avant d'imaginer une pièce il est important de connaître ses dimensions : longueur, largeur et hauteur. Il faudra que je fasse attention à ce que mes **pièces ne dépassent pas le plateau** qui est un carré : **20cm x 20cm** pour une hauteur de **22cm**. Cette contrainte ne va pas me permettre de faire des pièces plus grandes ou plus hautes que ces dimensions. Dans ce projet, la taille sera limitée mais elle peut être contournée en créeant plusieurs pièces qui peuvent s'assembler. 

![](images/limite_imp-min.jpg)

- Pas de support 

L'idée est de créer des prototypages rapides et sans déchets, pour ce faire je ne souhaite pas utiliser de supports lors de mes impressions. Cette contrainte, bien qu'intéressante, est un vrai challenge! Cette envie de ne pas créer de déchets va m'empêcher de créer des formes organiques ou des formes composées de ponts car elles nécessitent des supports importants pour éviter les petits problèmes. Cependant, voulant créer des formes géométriques simples, je pense ne pas rencontrer trop d'inconvénients.  

- Finition 

Lorsque l'on réalise un projet avec les imprimantes 3D, il est important de faire attention aux finitions. C'est également à cause de cette contrainte que j'ai décidé de ne pas créer de support lors de mes impressions car ceux-ci restent souvent coller aux impressions et crée pas mal d'imperfections. Si tout de même, il y a quand même des imperfections lors des impressions, il sera nécessaire d'y apporter des retouches manuelles pour tenter d'enlever le côté granuleux de la pièce. 

## Materiality : PLA

Le matériau utilisé pour ce projet final est le **PLA** (acier polylactique), un polyester à base **végétale biodégradable**. C'est un matériau peu coûteux, sans déformation importante et **facile à utiliser** pour l'impression 3D. Grâce à ce matériau, la production mise en place pour ce projet peut **se faire rapidement et à basse température**. Je peux soit imprimer des petits prototypes soit imprimer de grands objets occupant presque tout le plateau. Pour ce projet, je vais opter pour la deuxième solution.

Tout comme le studio UAU, je vise un **projet zéro déchet et zéro émission**. L'envionnement a une place importante et il faut y faire attention. C'est pour cette raison que j'ai décidé d'utiliser ce type de matériau. Je vais utiliser la même technique qu'eux en prennent le parti de ne créer **aucun matériau de support** pour éviter les déchets. De plus, l'idée du projet est de concevoir uniquement à la demande pour ne pas créer des prototypes en PLA qui ne serviront pas. 

![](images/pla_matérialité-min.jpg)

## Waterproofing in 3D 

On a souvent tendance à penser qu'une impression en PLA est étanche, mais ce n'est pas toujours le cas. Afin d'éviter des problèmes de fuites lorsque l'eau sera dans le vase, il est important de connâitre certaines règles pour la réalisation et de modifier certains paramètres avant d'exporter le g-code. 

- **Géométrie**

Tout d'abord, il faut prendre en compte la **géométrie**, la forme du modèle. Il faut faire attention à ne pas créer des parois épaisses qui ont besoin d'un remplissage car cela peut provoquer des irrégularités et donc toutes sortes de désagréments. Dans les réalisations à venir, il sera nécéssaire de créer des parois régulières. 

- **Périmètres** 

Il faut mettre au moins **3 ou 4 de périmètre** pour une seule paroi mais l'on peut encore l'augmenter jusque **5 ou 6 en fonction** de la forme du modèle.

- **Température (°)**

Afin d'être certain qu'il n'y aura pas de fuites, il faut rendre la température plus chaude pour assurer une bonne liasion entre les différentes couches. Il est conseillé d'augmenter la **température de buse de 5 à 10°**, voire utiliser la température la plus haute possible du plastique en question. 

- **Multiplicateur d'extrusion** (1)

Dans cette partie, il faut toucher au débit du filament et l'augmenter de **5 à 10% par rapport à la valeur par défaut**. Il y a aussi une autre manière de faire en définissant une largeur de ligne de 5 à 10% plus large (c'est-à-dire de 0,4 mm à 0,44 mm). 

- **Hauteur de couche** (2)

Il ne suffit pas d'avoir des hauteurs de couches fortement élevées, il faut également avoir des de très fines couches pour augmenter les chances de ne pas rater l'impression. Les impressions les plus durables ont été évaluées à une **hauteur de couche de 0,15 mm** mais selon certaines sources 0,2 serait mieux. 

**N.B.** : Pour imprimer de façon plus rapide combiné a des couches très épaisses, il faudra utiliser une buse plus large. Pour de meilleurs résultats, il est important de ne pas dépasser une hauteur de 60 - 65% à la largeur de la buse. 

![](images/étanchéité-min.jpg)

- **Chevauchement XY et autres réglages**

Si le modèle fuit encore malgré les suggestions précédentes, le chevauchement XY permet d'indiquer comment une ligne de remplissage va traverser le périmètre. **Par défaut**, il est a **25%** mais on peut l'augmenter de **40 - 50% si nécessaire**. 

# IDEA 1

**Fonctionnalité**

Pour récapituler, le projet est un vase qui est **modulable** grâce à l'assemblage de différentes pièces de **formes géométriques**. L'idée est de développer **4 types** de formes : le cercle, le carré, le rectangle et la demi-sphère. Cependant, je compte proposer plusieurs protoypes de ces formes en jouant sur les hauteurs. Il y aurait donc 8 types de pièces dans le catalogue. Grâce à cette façon de faire, le vase va acquérir au fur et à mesure des assemblages une **forme totémique** tout comme Sottsass qui fait référence à l'architecture d'autrefois. 

Toutes ces formes peuvent être imprimées en **différentes couleurs** : rose, mauve, bleu, vert, orange, rouge,... Comme expliqué auparavant, c'est l'utilisateur de l'objet du quotidien qui choisit tout : la forme, la couleur et l'assemblage. Il peut réaliser son vase comme il le souhaite. De plus, ce vase est pensé pour qu'il puisse être **utile à tout type de fleurs** : pour des fleurs à longues tiges ou à courtes tiges. Il suffit de jouer avec les pièces en en mettant plus ou moins. Le vase est également pensé pour que l'utilisateur puisse choisir si il veut y mettre un bouquet ou seulement quelques fleurs. Cette idée se travaillera à l'aide de différents "capuchon" sur le haut du vase, où il sera soit plus large soit plus étroit. Ce vase est divisé en trois partie : la base, la pièce intermédiaire et le capuchon. Chaque pièce géométrique peut être une des parties. 

**Type d'assemblage**

L'assemblage des différentes pièces se réfère au fonctionnement d'une vise dans un écrou. L'idée est de viser chaque pièce les unes dans les autres afin que l'assemblage ne bouge pas, qu'il soit le plus solide possible et surtout étanche. 

![](images/ASS-min.jpg)

**Colorimétrie** 

Ce projet a pour but d'offrir un grand nombre de possibilité de choix au sein de ses couleurs. Tout comme Ettore, ce vase sera coloré et remplit d'énergie et de positivité. C'est pour cette raison qu'il propose un large choix de couleurs que vous pouvez retrouvez ci-dessous :

![](images/palette_projet2-min.jpg)

**Types de formes** 

Comme je viens de l'énnoncé ci-dessus, le vase est divisé en **trois parties** :

1. _Pièce de type base_ : elle va permettre de contenir l'eau pour préserver les fleurs. Cette pièce se doit d'être plus lourd que les deux autres types de pièce car elle doit tenir tout le vase. 

2. _Pièce de type intermédiaire_ : les pièces intermédiaires sont les pièces qui vont permettre de choisir la taille du vase. Ce sont des pièces plus petites et plus fines que les autres afin de donner une certaine élégance au vase. 

3. _Pièce de type capuchon_ : le haut du vase va être soir plus large soit plus étroit en fonction du type de vase que l'on souhaite pour son bouquet (vase cylindrique, vase conique, vase soliflore,...).

- PIECE N°1

![](images/forme_1-min.jpg)

Cette première pièce est de type intermédiaire. Pour qu'elle puisse s'accrocher aux autres, elle doit avoir une ouverture sur sa base avec un filetage à l'intérieur pour qu'une autre pièce puisse se rattacher. Il en faut également une sur sa hauteur avec en plus une sorte de petit écrou rond qui va lui permettre de s'imbriquer dans une autre. Cette forme est, comme vous pouvez le voir, en forme de cercle avec un jeu de textures dessus de manière verticale pour avoir différentes sensations au toucher ainsi que pour lui donner du relief. 

Pour modéliser cette pièce sur fusion, je suis passée par plusieurs étapes en utilisant de nouveaux outils :

1. Afin de réaliser le cercle avec les textures, j'ai utilisé l'outil **Esquisse** dans Solide en choisissant l'outil **Cercle** pour y dessiner premièrement un cercle de  diamètre de 120mm. 

2. Pour réussir à faire la texture, j'ai dessiné sur la **même esquisse** des petites encoches de 5mm sur 5mm tout autour du cercle en utilisant l'outil **Ligne**. 

3. Ensuite, afin de offrir une dimension 3D à l'objet, j'ai utilisé l'outil **Extrusion** en lui donnant une hauteur de 50mm. 

4. Après la 4ème étape, j'ai réaliser les ouvertures de 50mm sur la base et le haut en utilisant l'outil **Perçage** pour traverser toute la pièce. 

5. Pour finaliser la pièce, j'ai réaliser les filetages pour pouvoir assembler les pièces. J'ai donc sélectionné par en dessous, l'intérieur de la pièce en utilisant l'outil **Filetages** en mettant les informations suivantes : j'actionne modélisé, je mets 50x3 pour la classe et j'y mets également la taille de l'ouverture (50mm) et de la hauteur (25mm).

- PIECE N°2
 
![](images/pièce_2-min-2.jpg)

Cette pièce-ci est également de type intermediaire. Son fonctionnement est le même que la première pièce : ouverture sur le bas et sur le haut avec un écrou pour s'embriquer dans une autre. Cette forme est un long tube avec une surface lisse cette fois-ci pour avoir un autre type de texture. 

Pour modéliser cette pièce sur fusion, je suis passée par presque toutes les mêmes étapes que la première pièce :

1. Premièrement, j'ai utilisé dans la partie **Solide** en cliquant sur le petit + vert, l'outil **Cercle** pour créer un cercle d'un diamètre de 100mm.

2. Deuxièmement, j'ai de nouveau utilisé l'outil **Extrusion** en donnant une hauteur de 100mm.

3. Troisièmement, j'ai choisi l'outil **Perçage** tout le long du tube avec un diamètre de 50mm. 

4. Quatrièmement, j'ai utilisé l'outil **Filetage** sur la base et le haut avec les mêmes mesures énnoncés dans la pièce 1.

- PIECE N°3

![](images/pièce_3-min-2.jpg)

Cette dernière pièce est de type base. Son fonctionnement est le même que la deuxième pièce : ouverture sur le bas et sur le haut avec un écrou pour s'embriquer dans une autre. La base est en forme de cube et plus massive que les autres pour être certaine que le vase soit très solide lorsque l'on va rajouter l'eau et les fleurs. Elle est également dotée d'une surface lisse pour avoir un juste milieu avec les différentes textures. 

Pour modéliser cette pièce sur fusion, je suis passée par presque toutes les mêmes étapes que la deuxième pièce :

1. Avant toute chose, j'ai sélectionné dans la partie **Solide** en cliquant sur le petit + vert, l'outil **Rectangle/Carré** pour créer un carré de 140mm de côté.

2. Après, j'ai utilisé l'outil **Extrusion** en donnant une hauteur de 60mm.

3. Troisièmement, j'ai choisi l'outil **Perçage** en lui donnant une prodonfeur de -40mm pour qu'il n'y ai pas de fuites dans le carré avec un diamètre de 50mm. 

4. Pour finir, j'ai utilisé l'outil **Filetage* uniquement sur le haut avec les mêmes mesures énnoncés dans la pièce 1 et 2.

> Réglages d'impressions 

Pour ces différentes pièces, les réglages ont été les mêmes. Afin que ce vase soit étanche, j'ai éffectué les réglages conseillé dans la partie recherche de projet ci-dessus :

1. Hauteur de couche : 0,2
2. Périmètres : 6
3. Couches solides : 6 et 6
4. Densité de remplissage : 10% 
5. Remplissage : rectiligne 
6. Boucles (jupe) : 3
8. Multiplicateur d'extrusion : 1.2 
7. Support : non
9. Température lors de l'impression : augmenter la buse de 5-10% 

![](images/réglage_idée_1-min.jpg)

> Impressions 

![](images/impressions_idée_1-min.jpg)

## CATALOGUE

![](images/Catalogue_-min.jpg)

## Examples of possible assemblies

![](images/vase_moche_final-min.jpeg)

# TEACHERS FEEDBACK 

Comme on peut le voir sur les photos ci-dessus, le vase est un objet beaucoup **trop massive**, **loin d'être élégant** et **pas très joli**. Le but premier d'un vase est de mettre en avant les fleurs à l'aide du vase. Cependant, dans cette proposition, on ne voit que le vase et il ne mets pas en valeur les fleurs. Pour rendre son élégance au vase, on m'a surgéré de **revenir sur l'idée du plug-in** expliqué au début du projet final : le tabouret modulable bleu. Cependant, grâce à ces prototypes, nous avons pu voir qu'il était intéréssant de **mettre plusieurs pièces l'une à côté de l'autre**.

# NEW IDEA 

**Fonctionnalité**

L'idée est de créer **trois types de bases** différentes pour trois types de bouquets : le bouquet sauvage (très dense), le bouquet dit "habituel, normal" et le bouquet qui se compose d'une ou deux fleurs (type soliflore). Ces bases sont des **tubes avec des hauteurs et des diamètres différents** : un grand, un moyen et un petit. L'utilisateur peut choisir de soit prendre qu'un seul type de vase, soit assembler deux vases ensemble ou encore trois. L'idée est la même qu'au début, l'utilisateur choisit tout : le tube ou les tubes, les formes et la couleur. En ce qui concerne les **formes**, c'est elles qui seront **modulables** dans le projet, elles viendront s'imbriquer autour des tubes. Ces formes seront des **anneaux (type gallets)** de différentes tailles, c'est-à-dire que soit on prend des pièces de la même taille ou que des différentes. J'ai décidé de finalement choisir **une seule forme géométrique**, **le cercle**, en congeant les cotés pour affiner le prototype. Cette version permettra au vase d'être plus fin, moins massif et plus élancé. 

**Type d'assemblage**

Cet assemblage est très simple! C'est comme enfiler une bague autour de son doigt. On prend les pièces et on les place l'une à la suite de l'autre en les glissant le long des tubes. 

![](images/ass2-min.jpg)

**Colorimétrie** 

Dans cette nouvelle idée, il y a toujours une variété de couleur mais elle est plus abordable et plus restreinte. Afin de rendre le vase moins massif et plus élégant, j'ai décidé d'utiliser des couleurs claires - pastels afin d'adoucrir les formes géométriques. Le projet propose 9 couleurs : le bleu, le rouge, le rose, le vert, le orange, le jaune, le gris, le lilas et le turquoise clair.

![](images/colour_idea_2-min.jpg)

**Types de formes**

Dans cette nouvelle idée, il y a **deux types de pièces** : les trois tubes avec des tailles et des diamètres différents et les anneaux autour de taille et diamètres différents. 

1. Les tubes : ils vont me permettre de servir de base pour l'eau et en même temps va permettre de régler le problème d'étanchéité car il sera effectué qu'en une seule pièce et non en plusieurs petites pièces.

2. Les anneaux : ils vont venir s'embriquer autour des tubes pour solidifier la structure. Il va également permettre à l'utilisateur de les disposer comme il le souhaite : toutes des pièces collées, des pièces espacées, des pièces différentes,...

- PIECE N°1 : TUBE

![](images/fusion_tube-min.jpg)

Ces tubes seront comme expliqué, les bases des trois vases et ils seront imrpimés en blanc. Ces longs tubes sont munies à leurs bases d'une fine petite plateforme qui va permettre de supporter les pièces qui vont être ajoutées autour de ceux-ci. De plus, le tube sera plein à sa base sur 200 mm afin de solidifier le vase. Ces tubes se veulent étanches et c'est pour cette raison que ces pièces et uniquement ces pièces seront étanches car les autres n'ont pas besoin de l'être. 

Dimensions des trois tubes : 

```
1. Tube grand : Ø 100mm, h = 200mm
2. Tube moyen : Ø 75mm, h = 180mm
3. Tube petit : Ø 50mm, h = 160mm
```
Réalisation des pièces sur fusion :

1. Pour commencer, j'ai utilisé l'outil **Cercle** dans esquisse en créant différents cercle avec des diamètres différents(100, 75, 50) en plan.

2. Ensuite, j'ai utilisé l'outil **Extrusion** pour donner les différentes tailles aux tubes (200,180,160) en coupe. 

3. Après, j'ai sélectionné l'outil **Perçage** pour créer les ouvertures pour les fleurs avec un diamètre de - 6mm à chaque diamètre (94,69, 44) et de -180, -160, -140 pour la profondeur du vase. 

4. Pour terminer, j'ai réutilisé l'outil **Cercle** pour créer la plateforme : 120mm, 95mm, 70mm pour chaque type de tubes et pour ensuite utilisé l'outil **Extrusion** avec une hauteur de 3mm. 

- PIECE N°2 : ANNEAU

![](images/anneaux_pièces-min.jpg)

Les anneaux sont imaginés avec différents diamètres et tailles, certaines sont plus grandes et d'autres plus petites pour créer plusieurs types d'effets. Ces pièces peuvent être imprimés soit de la même couleur, soit de deux couleurs ou encore de toutes les couleurs. Chaque pièce correecpond à à type de tube, il y a uniquement les tailles qui varient. Elles vont venir, comme expliqué ci-dessous, habiller les tubes et créer un jeu de couleur. 

Réalisation des pièces sur fusion :

1. Tout comme la plupart des pièces, c'est le même commencement : sélectionner l'outil **Cercle** dans esquisse et créer les différents diamètres de pièces qui sont au choix.

2. Après, il faut utiliser l'outil **Extrusion** pour donner la dimension 3D à la pièce. Les pièces ont toutes la même hauteur : 20mm.

3. Ensuite, je sélectionne l'outil **Perçage** pour créer les ouvertures sur le bas et le haut qui traverse toute la pièce en créant un trou en fonction du diamètre de la pièce (100mm, 75mm ou 50mm).

4. Pour finir la pièce, il faut utiliser l'outil **Congé** avec une dimension de 10 mm pour donner une forme de gallet à la pièce et la pièce est terminée, rien de bien compliqué!  

> Réglages d'impressions

- Réglages des tubes : 

Pour ces trois pièces, les réglages ont été les mêmes que pour les pièces massives d'autrefois. Afin que ce vase soit étanche, j'ai éffectué les réglages conseillé dans la partie recherche de projet ci-dessus :

1. Hauteur de couche : 0,2
2. Périmètres : 6
3. Couches solides : 6 et 6
4. Densité de remplissage : 10% 
5. Remplissage : rectiligne 
6. Boucles (jupe) : 3
8. Multiplicateur d'extrusion : 1.2 
7. Support : non
9. Température lors de l'impression : augmenter la buse de 5-10% 

![](images/réglage_idée_1-min.jpg)
- Réglages des anneaux :

Ces anneaux n'ont pas besoin d'autant de réglages que pour les tubes car les pièces n'ont pas besoin d'être étanche car elle n'auront aucun contact avec l'eau. J'ai donc éffectué les réglages habituels :

1. Hauteur de couche : 0,2
2. Périmètres : 3
3. Couches solides : 3 et 3
4. Densité de remplissage : 10% 
5. Remplissage : rectiligne 
6. Boucles (jupe) : 3
7. Support : non 
![](images/réglages_anneaux-min.jpg)

> Impressions

![](images/impressions_idée_2-min.jpg)

![](images/impressions_idée_2.2-min.jpg)

**N.B** : pour les impressions rouge et orange, j'ai du **exceptionnellement mettre de légers supports** car les imprimantes étaient toutes occupées : il a fallut les partager. Nolwenn avait besoin de support donc j'ai du en mettre. 

## CATALOGUE

![](images/tubes_photos-min.jpg)

![](images/catalogue_idée_2-min.jpg)

![](images/cata2-min.jpg)

![](images/catalo3-min.jpg)

## Examples of possible assemblies

- Vase type soliflore 

![](images/vase_5-min.jpg)

- Vase "habituel"

![](images/vase_7_5-min.jpeg)

- Vase pour fleurs sauvages 

![](images/vase_10-min.jpg)

- Vase assemblé seul, par deux ou par trois

![](images/ensemble_vase-min.jpg)

## TECHNICAL SHEET

![](images/TECHNICAL_SHEET-min.jpg)

# PRÉ-JURY (17.12.2020)

Après cette correction, il faut améliorer plusieurs points dans la conception/réalisation des trois vases : 

- Faire un travail plus approfondi sur les **couleurs** (une plaquette de couleurs plus variée? Revenir sur les couleurs d'origine du tabouret Pilastro?)

- Trouver une solution d'**assemblage** entre les différentes bien. Actuellement lorsqu'on prenait l'un des vases les pièces ne collaient pas entre elles et tout tombait. 

- Réfléchir à une **taille plus grande** pour le soliflore. Le but d'un soliflore étant de sublimer la fleur en question, il faut donc que le vase soit plus grand pour permettre d'avoir une belle et haute fleur, avec une plus longue tige.


# THE PROJECT

## FONCTIONNALITE

L'idée est toujours la même mais avec de nouvelles idées supplémentaires afin de perfectionner le design des trois vases. L'objet de design est toujours un lot de trois vases, de deux vases ou encore un vase tout seul. Il existe donc 3 vases de différentes hauteurs (20cm/18cm/16cm) et diamètres (10cm/7,5cm/5cm). Le plus grand est destiné aux bouquets sauvages ou aux bouquets avec de longues tiges, le moyen est conçu pour les fleurs de la vie quotidienne et le plus petit est un soliflore qui va permettre de sublimer la ou les deux fleurs. Ces vases sont créer par les utilisateurs eux-mêmes, ils vont pouvoir choisir les couleurs, la taille et le diamètre des différentes pièces imaginées. L'utilisateur choisit le "tube" voulu puis choisi les pièces qu'il veut mettre autour du tube pour créer lui-même son vase. 

## DESIGN DU VASE 

Ce design a été imaginé en deux parties : 

1. Un **hommage** aux tabourets pilastro de deux façon :

- Réutiliser les couleurs imaginées pour le tabouret, c'est-à-dire, le lila, le rose, le rouge, le vert et le noir. 
- Création d'une pièce ressemblant aux bases pour séparer les différentes pièces pour créer une forme totémique. 

2. **Customiser** l'objet initiale avec plus de couleurs 

- Offrir différents types de pièces pour créer son propre vase.
- Offrir une nouvelle palette de couleurs pour avoir un plus large champ de possibilités de vases.

## NOUVEL ASSEMBLAGE

L'assemblage autour de la base est **toujours le même**, il suffit d'enfiler la pièce comme une bague autour d'une doigt. Cependant, afin de ne pas tout faire tomber lorsque l'on porte le vase, j'y ai ajouté une **fonction supplémentaire** : l'idée est de pouvoir emboiter chaque pièce l'une dans l'autre à l'aide de petits carrés qui ressortent de la pièce au dessus et des trous à la taille des carrés du dessus en dessous. C'est tout simple, c'est comme des **clips**. Etant donné que les vases sont modulables, ce sera toujours le même type d'accroches entre les pièces mais elles ne seront pas toujours placées au même endroit. 

![](images/assemblage_new-min.jpg)

## NOUVELLE COLORIMETRIE

Afin de garder un lien fort avec l'objet d'origine, j'ai décidé de proposer dans un premier temps les mêmes couleurs que celui d'origine : vert, rose, lila, rouge et noir pour créer un hommage à ce dernier. Ensuite, pour lui ajouter une nouvelle dimension, j'ai décidé d'élargir la palette de couleurs que j'avais proposé aupparavant que vous pouvez retrouver ci-dessous. Les couleurs d'hommages peuvent soit s'utiliser ensemble ou se mélanger avec les couleurs "custom", tout est au choix, il ne faut pas oublier que c'est l'utilisateur qui décide tout!

- Hommage au tabouret Pilastro de Ettore Sottsass 

![](images/hommage-min.jpg)

- Custom 

![](images/couleur_fin-min.jpg)

## BASES ETANCHES

![](images/vases_bases_finaux-min-min.jpg)

Les tubes seront comme expliqué auparavant, les bases des trois vases et ils seront imrpimés en blanc. Ces longs tubes sont munies à leurs bases d'une fine petite plateforme avec des clips qui ressortent et vont permettre de supporter les pièces qui vont être ajoutées autour de ceux-ci. De plus, le tube sera plein à sa base sur 200 mm afin de solidifier le vase. Ces tubes se veulent étanches et c'est pour cette raison que ces pièces et uniquement ces pièces seront étanches car les autres n'ont pas besoin de l'être. 

- Grande base pour bouquets sauvages (gauche)

`h = 20 cm - Ø = 100 cm - Ø = 0,4 cm`

- Base intermédiaire pour des bouquets de la vie de tous les jours  (centre)

`h = 18 cm - Ø = 75 cm - Ø = 0,4 cm`

- Base soliflore pour 2-3 belles fleurs (droite)

`h = 16 cm - Ø = 50cm - Ø = 0,4 cm`

> Comment réaliser ces pièces sur Fusion 360?

1. Pour commencer, j'ai utilisé l'outil **Cercle** dans esquisse en créant différents cercle avec des diamètres différents(100, 75, 50) en plan.

2. Ensuite, j'ai utilisé l'outil **Extrusion** pour donner les différentes tailles aux tubes (200,180,160) en coupe. 

3. Après, j'ai sélectionné l'outil **Perçage** pour créer les ouvertures pour les fleurs avec un diamètre de - 6mm à chaque diamètre (94,69, 44) et de -180, -160, -140 pour la profondeur du vase. 

4. Par la suite, j'ai réutilisé l'outil **Cercle** pour créer la plateforme : 120mm, 95mm, 70mm pour chaque type de tubes et pour ensuite utilisé l'outil **Extrusion** avec une hauteur de 3mm.

5. Pour terminer, je crée avec l'outil **Esquisse** sur la plateforme du haut (qui ressortent) et du bas (qui rentrent) des petits carrés de 2 mm sur 2 mm à un diamètre de 119,2 cm (pour le grand vase), de 94,2 cm (pour le vase intermédiaire) et de 69.2 (pour le soliflore).

> Réglages d'impressions

Comme expliqué auparavant, les réglages sont les mêmes que pour les pièces massives d'autrefois. Afin que ce vase soit étanche, j'ai éffectué les réglages conseillé dans la partie recherche de projet ci-dessus :

1. Hauteur de couche : 0,2
2. Périmètres : 6
3. Couches solides : 6 et 6
4. Boucles (jupe) : 3
5. Multiplicateur d'extrusion : 1.2 
6. Support : non (toujours dans l'idée de ne pas gaspiller)
7. Température lors de l'impression : augmenter la buse de 5-10% 
![](images/réglage_idée_1-min.jpg)

> Impressions

![](images/final_impressions_blanc_base-min-min-min.jpg)

> Echecs d'impressions 

![](images/echec_base1-min.jpg)
![](images/echec_base2-min.jpg)

> Lorsque l'échec mène à une bonne surpise...

![](images/echec_base3-min.jpg)

Sur ce vase ci, l'impression a eu un problème mais a créé quelque chose d'intéressant : une texture comme du crépis, une belle texture. Bien que celle-ci ne rentre pas dans le projet qui se veut "kitch" et en rapport avec le Pop Art, je me suis dis que c'était une autre piste intéréssante que je pouvais poursuivre par la suite pour le plaisir. 

## TYPES D'ANNEAUX

- **Pièce 1 (pièce dite "capuchon")**

![](images/pièce_1capuchon-min.jpg)

1. Hauteur : 0,5 cm ou 1 cm ou 2 cm 
2. Diamètre du centre : 5,7 cm ou 7,7 cm ou 10,7 cm
3. Grand diamètre : 9,5 cm ou 120 cm ou 140 cm
4. Trous renfoncés en bas : 2,5 x 2,5 cm x 3,5 cm à 69,2 cm ou 94,2 ou 119,2 du bord de l'intérieur

- **Pièce 2 (pièce dite "normale/intermédiaire")**

![](images/pièce_2_normale-min.jpg)

1. Hauteur : 0,5 cm ou 1 cm ou 2 cm 
2. Diamètre du centre : 5,7 cm ou 7,7 cm ou 10,7 cm
3. Grand diamètre : 9,5 cm ou 120 cm ou 140 cm
4. Trous ressortis : 2,5 x 2,5 cm x 3,5 cm à 69,2 cm ou 94,2 ou 119,2 du bord de l'intérieur
5. Trous renforcés en bas : 2,5 x 2,5 cm x 3,5 cm à 69,2 cm ou 94,2 ou 119,2 du bord de l'intérieur

- **Pièce 3  (pièce "dédoublement du tube")**

![](images/pièce_tube-min.jpg)

1. Hauteur : 1 cm ou 2 cm ou 4 cm
2. Diamètre du centre : 5,7 cm ou 7,7 cm ou 10,7 cm
3. Grand diamètre : 0,4 cm
4. Trous ressortis : 2,5 x 2,5 cm x 3,5 cm à 0,80 du bord de l'intérieur
5. Trous renforcés en bas : 2,5 x 2,5 cm x 3,5 cm à 0,80 du bord de l'intérieur

- **Pièce 4 (pièce pour "en dessous dédoublement du tube")**

![](images/pièce_4_cata-min.jpg)

1. Hauteur : 0,5 cm ou 1 cm ou 2 cm
2. Diamètre du centre : 5,7 cm ou 7,7 cm ou 10,7 cm
3. Grand diamètre : 9,5 cm ou 120 cm ou 140 cm
4. Trous ressortis : 2,5 x 2,5 cm x 3,5 cm à 0,80 du bord de l'intérieur
5. Trous renforcés en bas : 2,5 x 2,5 cm x 3,5 cm à 69,2 cm ou 94,2 ou 119,2 du bord de l'intérieur

- **Pièce 5 (pièce pour "au dessus du dédoublement tube")**

![](images/pièce_5_oko-min.jpg)

1. Hauteur : 0,5 cm ou 1 cm ou 2 cm
2. Diamètre du centre : 5,7 cm ou 7,7 cm ou 10,7 cm
3. Grand diamètre : 9,5 cm ou 120 cm ou 140 cm
4. Trous ressortis : 2,5 x 2,5 cm x 3,5 cm à 69,2 cm ou 94,2 ou 119,2 du bord de l'intérieur
5. Trous renforcés en bas : 2,5 x 2,5 cm x 3,5 cm à 0,80 du bord de l'intérieur

> Réglages d'impressions

Comme expliqué auparavant également, les pièces n'ont pas besoin d'être étanche (donc pas les mêmes réglages) car elle n'auront aucun contact avec l'eau. J'ai donc éffectué les réglages habituels :

1. Hauteur de couche : 0,2
2. Périmètres : 3
3. Couches solides : 3 et 3
4. Densité de remplissage : 10% 
5. Remplissage : rectiligne 
6. Boucles (jupe) : 3
7. Support : non 
![](images/réglages_anneaux-min.jpg)

> Impressions

![](images/impressions_finale_couleurs-min-min-min.jpg)
![](images/impressions2_couleurs-min-min.jpg)

> Echecs d'impressions 

![](images/echec-min.jpg)

## CATALOGUE FINAL

- Tubes - bases

![](images/cata17-min.jpeg)
![](images/cata15-min.jpg)
![](images/cata14-min.jpg)

- Pièces pour le soliflore (hauteur : 1 cm)

![](images/cata1.jpg)
![](images/cata2-min-3.jpg)
![](images/cata4-min-2.jpg)

- Pièces pour le vase intermédiaire (hauteur fine pièce : 0,5 cm - hauteur pièce normale : 1 cm - hauteur pièce grosse : 2 cm)

![](images/cata8-min.jpg)
![](images/cata9-min.jpg)
![](images/cata10-min.jpg)
![](images/cata11-min.jpg)

- Pièces pour le grand vase (hauteur petites pièces : 1 cm - hauteur tube : 4 cm)

![](images/cata5-min.jpg)
![](images/cata6-min.jpg)
![](images/cata7-min.jpg)

- Couleurs et monochrome

![](images/sans_f-min.jpg)
![](images/bases-min.jpg)

## Examples of final possible assemblies

- Soliflore

![](images/p1-min.jpg)
![](images/p2-min.jpg)

- Intermédiaire

![](images/p3-min.jpg)
![](images/p4-min.jpg)

- Grand

![](images/v_bleu-min.jpg)

- Trilogie

![](images/1111-min.jpg)
![](images/11-min.jpg)

## FINAL TECHNICAL SHEET

![](images/ok_impression-min-compressed.jpg)

## THE END

Cette image regroupe tout le travail, le processus réalisé pendant le quadrimestre! 

![](images/fin-min.jpg)
