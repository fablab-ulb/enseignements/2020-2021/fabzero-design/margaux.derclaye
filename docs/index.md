## DESIGN DIARY

Si tu es intéressé(e) par mon travail, n'hésite pas à le suivre!

## About me

![](images/avatar-photo.jpg)
© M.D   

Hello, je m'appelle **Margaux** Derclaye, j'ai 21 ans. Je suis étudiante en **première Master** à l'Université Libre de Bruxelles en Architecture à la faculté de la Cambre Horta. Ayant toujours été intéréssée par le graphisme et le design, j'ai choisi de suivre l'option Architecture et Design afin d'apprendre de nouveaux outils et de perfectionner mes compétences.

En plus de mes études, j'ai toujours fait beaucoup de sport. Je joue depuis 15 ans au hockey et j'ai, depuis peu, commencé le foot avec une nouvelle équipe. Depuis mes 5 ans, j'ai toujours été investie dans les mouvements de jeunesse et anime depuis maintenant 4 ans. 

## My background
Avant d'arriver en troisième année de Bachelier à l'ULB, j'ai d'abord effectué mes deux premières années d'Architecture à **LOCI - l'Université Catholique de Louvain** à Bruxelles. J'ai décidé de changer d'université car j'avais besoin de m'épanouîr d'un point de vue artistique, créatif. La Cambre Horta offre un plus grand choix en ce qui concerne les projets ainsi qu'un choix d'options plus varié. Je suis très contente aujourd'hui d'avoir pu me former dans ces deux universités. J'ai acquis à Loci une théorie importante en ce qui concerne l'architecture et une liberté créative au sein de l'ULB. 

Cette année, je devais partir pendant 10 mois en Erasmus dans l'Université IUAV de Venise, en Italie. Cependant, j'ai décidé d'annuler cet échange suite à la situation actuelle : le covid. Pensant partir en Erasmus, j'ai pris des cours d'italien pendant 5 mois de début févrirer à fin juin à l'Ambassade d'Italie à Bruxelles pour atteindre le nivezu A1-A2.

## My skills 

- AutoCad
- SketchUp
- Photoshop
- Indesign 
- Lightroom
- Daylight Visualizer
- Rhinoceros 3D

### Project

> Objet choisi au Design Museum Brussels : 

**Tabouret Pilastro, by Ettore Sottsass - Kartell** 


- Informations générales : 

Collection : Kartell - 2015

Designer : Ettore Sottsass - 2005

Couleur : lilas

Matière : Technopolymère thermoplastique teinté dans la masse

Dimensions : Hauteur : 46 cm x Ø : 35 cm

Poids : 3,5 kg

- Description de l'objet : 

En 2015, la collection « Kartell goes Sottsass - A Tribute to Memphis » lance trois pièces inédites conçues par le maître du Design Ettore Sottsass afin de lui rendre hommage. Dans cette collection se trouvent les tabourets Pilastro et Colonna ainsi que le vase Calice. L'objet que je vais analyser tout au long du quadrimestre est le tabouret Pilastro. Au musée, le tabouret est exposé en lilas mais il existe en quatre autres couleurs : rouge, violet, vert et noir. Il s'inspire de l'architecture antique grâce à ses lignes réalisées en technopolymère thermoplastique teintées dans la masse peint. Cet objet superpose des formes géométriques simples, joue avec la couleur et a une silhouette totémique. Ce tabouret se distingue des autres par sa polyvalence : il peut soit être utilisé comme un tabouret soit comme table d'appoint. 

- Choix de l'objet : 

J'ai choisi cet objet pour ses formes géométriques pures et sa simplicté. Ce qui m'intéresse plus particulièrement dans cet objet c'est qu'il est multifonctionnel. C'est à la fois un objet d'art excentrique par sa couleur mais également un objet quotidien : une table d'appoint et un tabouret. Après m'être renseignée, j'ai appris que le designer s'était inspiré _des bandes dessinées, du Pop art, du cinéma, du Kitsch ou de l'Art déco_, ce qui permet de laisser libre cours à l'imagination...

![](images/sample-pic-2.jpeg)
© M.D   



