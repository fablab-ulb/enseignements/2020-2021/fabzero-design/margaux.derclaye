# MODULE 1 : Fusion and GitLab

Dans ce module 1, j'ai débuté avec un programme que je ne connaissais pas : **Fusion 360**, un logiciel qui permet de modéliser/designer des objets. J'ai également appris à utiliser **GitLab**, une plateforme qui va me permettre de créer mon propre espace de travail où toutes mes démarches y seront expliquées et mes travaux y seront répertoriés. Cet outil va permettre de faire voir aux autres mon travail et mon évolution. Il me permet également de revoir mes démarches en permanence.

Cependant, j'ai du me débrouiller par moi-même et avec l'aide d'autres étudiants pour ce premier module car je n'ai pas pu y participer suite à un contact avec une personne positive au Covid.

Lors de ce module 1, les étudiants ont suivi deux formations : celle de GitLab avec Denis Terwagne et celle de Fusion 360 avec Thibault.

## Research about Fusion 360

Afin de comprendre ce programme, j'ai regardé les deux vidéos Teams; celle du cube et de la pièce d'échec, réalisées lors du premier cours afin de rattraper mon retard. Comme certains outils n'apparaissaient pas lors du visionnage des vidéos Teams, j'ai également regardé un tuto sur [Fusion 360](https://www.youtube.com/watch?v=ExHGnBdvz7o) pour comprendre au mieux les commandes de celui-ci.

Étant plutôt habile et rapide avec les outils numériques, je me suis vite rendu compte que ce logiciel était assez similaire à _Autocad_ et _Sketchup_, que je connais bien. Selon moi c'est un mélange des deux programmes.

## Configuration of Git

Afin de pouvoir utiliser GitLab directement sur mon ordinateur sans passer par le site, je dois configurer Git. Comment faire? Il suffit de suivre les instructions données sur GitLab via ce [lien](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#configure-git). Je vais tout de même vous les expliquer ci-dessous.

Tout d'abord je vais vérifier dans mon **Terminal** que Git est bien installé sur mon ordinateur. Pour ce faire je tape `git --version` dans mon Terminal.

Malheureusement mon terminal affiche `xcode-select: note: no developer tools were found at '/Applications/Xcode.app', requesting install. Choose an option in the dialog to download the command line developer tools.`, ce qui veut dire que la suite d'outils de développement n'est pas installée. J'appuie alors sur le bouton **Installer** et sur le bouton **Accepter** pour pouvoir accéder à la suite d'outils.
![](../images/git_configu.jpg)
Cependant, l'installation n'est pas possible car mon ordinateur me dit : _impossible d'installer ce logiciel car il n'est pas disponible actuellement depuis le serveur de mise à jour de logiciels_.

De ce fait, il va falloir trouver une solution. Je vais installer Git sur mon ordinateur. Je me rends sur le site [Git](https://git-scm.com) et télécharge la version **2.27.0 for Mac**. En cliquant sur télécharger, je suis dirigée vers une autre page, j'appuie alors sur **Binary installer**. De ce [site](https://sourceforge.net/projects/git-osx-installer/), j'installe  `Git-osx-installer` en appuyant sur **Download** pour télécharger Git sur mon ordinateur. Une fois installé, j'ouvre le programme et je le lance.
![](../images/Git_site_installation.jpg)

Cependant, il ne veut pas s'ouvrir et mon ordinateur affiche : _impossible d'ouvrir "Git-2.15.0-intel-universal-mavericks.pkg" car cette application provient d'un développer non identifié_. Pas de panique, rien de grave. Comment arranger le problème? Je vais dans **Paramètres** de mon ordinateur pour débloquer cette application provenant d'Internet. Ensuite, j'appuie sur **Sécurité et confidentialité** --> **Général** --> **Cadenas** (pour pouvoir modifier le contenu) --> **Ouvrir quand même Git**.

Après toute ces manipulations, Git est enfin installé! Je vais tout de même vérifier pour être sûre, je tape alors dans ma console la même chose qu'au début : `git --version` pour voir si il trouve la commande. Effectivement la console trouve ceci : `git version 2.15.0`.

Une fois Git installé, j'arrive à la dernière étape pour le configurer. Afin de donner un nom à la personne qui utilise Git (moi), je vais taper avec mes informations personnelles les commandes suivantes dans ma console :
![](../images/Commande_configuration.jpg)

```
MBP-de-Margaux:~ margauxderclaye$ git config --global user.name "MBP Margaux Derclaye"

MBP-de-Margaux:~ margauxderclaye$ git config --global user.email "margaux.derclaye@ulb.be"
```
Pour terminer, je vérifie si la configuration a bien été effectuée en tapant la dernière commande : `git config --global --list`
```
MBP-de-Margaux:~ margauxderclaye$ git config --global --list

user.name=MBP Margaux Derclaye

user.email=margaux.derclaye@ulb.be
```
Et voilà, c'est terminé pour la configuration!

## Key SSH

Dans cette partie, je vais vous expliquer comment configurer la clé SSH. Pourquoi doit-on la configurer et à quoi sert elle? C'est simple, cette clé nous permet d'établir une connexion sécurisée entre notre ordinateur et le GitLab.

Pour commencer, il faut créer cette clé. Je vais aller dans **User Settings** dans GitLab --> **SSH** puis appuyer sur **Generate one**. Ce qui nous renvoie directement [ici](https://docs.gitlab.com/ee/ssh/README.html#generating-a-new-ssh-key-pair), le site de GitLab qui nous explique comment procéder. Sur ce site, je suis les instructions de **RSA SSH keys**.

Afin de créer la clé, j'encode dans mon terminal ceci : `ssh-keygen -t rsa -b 2048 -C "email@example.com`. Ma console affiche ceci : `Generating public/private ed25519 key pair`. Il me demande ensuite où je veux enregistrer le fichier. Par défaut il l'installe dans **(/Users/margauxderclaye/.ssh/id_rsa)** sur mon ordinateur. Comme je suis d'accord je fais **Enter**. Après cette étape, je retape sur **Enter** pour générer un code par défaut.

Voici tout ce que j'ai obtenu sur mon **Terminal** pendant les étapes énoncées ci-dessus :
```
The default interactive shell is now zsh.
To update your account to use zsh, please run `chsh -s /bin/zsh`.
For more details, please visit https://support.apple.com/kb/HT208050.
MBP-de-Margaux:~ margauxderclaye$ ssh-keygen -t rsa -b 2048 -C "email@example.com"

Generating public/private rsa key pair.
Enter file in which to save the key (/Users/margauxderclaye/.ssh/id_rsa):
Created directory '/Users/margauxderclaye/.ssh'.
Enter passphrase (empty for no passphrase):

Enter same passphrase again:
Your identification has been saved in /Users/margauxderclaye/.ssh/id_rsa.
Your public key has been saved in /Users/margauxderclaye/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:zS79Jn3FK22OPKVPXyx/dMz9zOnPCrXdVZ3HLooe1q0 email@example.com
The key's randomart image is:
+---[RSA 2048]----+
|                 |
|               .o|
|               .=|
|         o     .o|
|        S o   o++|
|         o o + *@|
|        . *.+ B*X|
|         +.o+*+OB|
|          .oE+O=B|
+----[SHA256]-----+
```
Après, je me rends dans le dossier que j'ai créé sur mon ordinateur nommé **SSH** et j'ouvre avec un éditeur de texte (TextEdit) le dossier **id_rsa.pub** (clé publique) afin d'accéder au contenu et de le copier :
```
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDnUgi9AU9j5sGKUSdqiTQvjeYtqA4CmGlaOKuk+AtcG3Qsoe8c8VEXwC/h/

iOIqpNw7I4sxfX9sdEMaz1S1c3Sp3RToajBgJqhJ2Glzmp8e/Z5ciZkkSRymqruFTh0KPeqQAMaXzQiAaGpOoLmbUkwzs3EHe/

x1ugJQbVLveDCFEuxh4CREWEfDKj7Cmi27zDHANlBZMvpzB89dSxQkS+Vmw0i+izCGITW3aWTObHkqx0aLWayHlnEpKMJmVEHwXIAOQx6FmO8iteSu5ofkhEwDxdysOc0

+hbPp6f/2pJCwAkIA84OibfNd0GvAI6doV7sbfd0iTursJnU2azPpDlVemail@example.com
```

Pour terminer la configuration de la clé SSH, je retourne dans **User Settings** puis clé **SSH**, ce qui affiche ceci :
![](../images/add_keys.jpg)
Dans **Key** je vais coller ce que j'ai copié ci-dessus et ensuite dans **Title** je vais choisir un nom : Macbook de MargauxD (pas obligatoire).

Pout finir, j'appuie sur **Add Key** et c'est terminé! La clé est créée et configurée!
![](../images/SSH.jpg)

## Configuration of Atom

Afin de pouvoir utiliser et travailler mes modules sur Atom, je dois premièrement télécharger le programme sur mon ordinateur. Pour ce faire, c'est tout bête, je me rend sur le site d'[Atom](https://atom.io) et appuie sur **Download**. Rien de plus simple! Cependant, afin de pouvoir utiliser l'application directement sur mon ordinateur, je dois faire quelques manipulations que je vais vous expliquer.

Je commence par créer un **Dossier** sur mon ordinateur avec toutes mes informations de GitLab. Pour réaliser cela, je **clone with SSH**, c'est-à-dire copier l'url qui envoie vers mon projet :
![](../images/clone_ssh.jpg)
Après avoir copié cet url, j'écris `cd Desktop` dans mon **Terminal** pour accéder au Bureau et `mkdir design`pour créer un dossier dessus. Pour y mettre mes informations GitLab dedans j'écris `git clone + mon url que j'ai copié auparavant` + **Enter**. Si la clé SSH n'avait pas été créer je n'aurai pas pu mettre mes informations sur mon ordinateur.

Mon terminal me demande si je suis sûre de vouloir me connecter à ce server, j'encode alors **yes**. Les fichiers se sont alors téléchargés dans mon dossier **Design**.

Pour la dernière étape de configuration, j'ouvre **Atom** --> **Open a project** --> **Sélectioner le dossier** et tous mes fichiers sont disponibles sur Atom. Je peux travailler sur Atom et envoyer les informations directement sur GitLab en appuyant sur Push après avoir enregistré mes modifications.

Voici ce que j'obtiens sur Atom :
![](../images/Vision_Atom.jpg)

GitLab et Atom sont liés. Si je travaille d'abord sur GitLab et que je veux par la suite travailler sur Atom, je dois d'abord appuyer sur **Fetch** puis sur **Pull** dans Atom pour envoyer les modifications que j'ai effectuées sur Atom. Inversement, quand j'ai fini de travailler sur Atom et que je veux envoyer mes modifications sur GitLab, je dois faire : **Save** --> dans **Unstaged Changes** je dois faire **Stage All** --> **Commit to master** --> **Push** et le tour est joué!

## Markdown for GitLab

Cette partie va vous permettre, tout comme ça l'a été pour moi d'enrichir le contenu de mes modules. Je vais vous expliquer les différentes commandes et les différents outils que nous pouvons utiliser.

1. **Insérer du texte**

Comme je vous l'ai expliqué ci-dessus, je peux soit ajouter du texte dans l'application Atom soit directement sur GitLab. Sur **GitLab**, je me rends dans le module que je veux modifier et j'appuie sur **Edit**. Dans **Write**, je peux écrire tout ce que je souhaite et dans **Preview** je peux voir comment le texte va s'afficher quand je l'aurai enregistré. Dans **Atom**, après avoir ouvert le fichier que vous souhaitez (par exemple : module 02), il vous suffit simplement d'écrire ce que vous souhaitez dans le noir (voir photo ci-dessus). La différence avec GitLab c'est que Atom nous permet de voir directement comment ça va s'afficher car l'endroit pour écrire et pour voir le rendu sont l'un à côté de l'autre.

2. **Insérer une image**

Afin de compléter nos informations et nos recherches, nous pouvons l'agrémenter avec des images. Je me rends donc sur mon profil **"margaux derclaye"**, je clique sur **Docs** et ensuite je vais dans le dossier **Image**. Une fois dans ce dossier, j'appuie sur le **+** et ensuite sur **Upload File**.
![](../images/Image__how_to_do_.jpg)
Une fois que l'image est chargée, il faut retourner dans le module souhaité pour y insérer `![](../images/(nom du fichier))`. Le fichier doit toujours être écrit soit en **jpg** soit en **jpeg**. Lorsque l'on importe une image, il est bien important de les compresser avant de les importer ainsi que de réduire leur taille en passant par **Photoshop**.

3. **À quoi servent les commits messages?**

Avant d'enregistrer nos modifications, nous pouvons donner un nom à ces modifications dans **Commit message**. Pourquoi c'est important/intéressant de le faire? Tout comme expliqué plusieurs fois au début de ce module, GitLab nous permet de toujours avoir une trace de ce que nous avons effectué. En écrivant la modification que vous avons effectuée, lorsque nous voulons retrouver quelque chose, nous pouvons grâce à cette commande retrouver directement ce que nous avons perdu dans **Repository** --> **Commit**.

4. **Outils Markdown**

Afin de rendre notre rendu plus clair, plus fluide, il existe une série de commandes pour y parvenir :

```
- **(texte)** pour donner un caractère gras
- _(texte)_ pour donner un caractère italique
- >(texte) pour insérer une note/citation
- `(texte)` pour insérer un code
- [](lien) pour insérer un lien
- -(texte) pour insérer une bulle noire
- 1.(texte) pour insérer des nombres
- - [ ](texte) pour insérer une liste --> faire [x] pour choisir cette partie
- ~~(texte)~~ pour barrer le mot
- ```(texte)``` pour encadrer le texte
```

## Activate email notifications

Il est important d'activer les notifications par E-mail afin de toujours recevoir les informations nécessaires sans devoir passer par le site. Afin de recevoir les notifications, il faut le configurer nous-même.

Je me rends alors sur mon profil en cliquant sur ma **photo de profil** (en haut à droite) puis je clique sur **Settings**. Ensuite dans la colonne de gauche je clique sur **Notifications. Une fois que je suis sur la page, je dois tout d'abord mettre **Participer** au lieu de Regarder dans **Niveau de notification globale**. après je dois cocher **Recevez des notifications sur votre propre activité**. Ce qui me permettra de ne pas recevoir les informations qui ne m'intéressent pas.

De ce fait, je vais remplacer **Global** par **Watch** dans seulement deux parties car les autres ne m'intéressent pas. Maintenant je vais recevoir les notifications par E-mail pour mon profil "margaux derclaye" et "coordination".
![](../images/notifications.jpg)

## Configuration of the website

Une fois que nous sommes plus à l'aise avec le site, nous devons le configurer en mettant nos informations personnelles mais nous pouvons également améliorer l'ensemble visuel en changeant le thème ou la couleur par exemple.

Pour configurer nos informations personnelles, il faut se rendre sur son **Profile** --> **Repository** --> **mkdocs.yml**. Tout comme le texte, il faut appuyer sur **Edit**. Il faut alors remplacer les données par défaut par les nôtres. Pour cette partie, je n'ai donc changé que les 5 premières lignes.
![](../images/infromations_perso.jpg)

Je peux également configurer le thème, la couleur et la typographie. Pour rendre mon site web plus attractif, c'est donc ce que je vais faire. Grâce à ce [site](https://www.mkdocs.org/#installation), j'ai pu changer le thème de mon profil. Pour l'installer, je vais encore dans **Profil** --> **Repository** --> **mkdocs.yml**. Une fois sur cette page, j'écris **Minty** au lieu de **spacelab** pour configurer le nouveau thème que j'ai choisi [ici](https://mkdocs.github.io/mkdocs-bootswatch/#minty). J'ai également désactivé les **highlightjs** en écrivant **false** pour éviter de mettre des couleurs à tout va dans mes blocs de code.

![](../images/Thème.png)

Si tu veux voir ce que ca donne clique [ici](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/margaux.derclaye/)!

## Check the site configuration

Tous les mardi et mercredi à 00h00, le site se met à jour automatiquement. Afin de voir que toutes nos modifications ont bien été effectuées lors de la mise à jour, je me rends sur mon **Profil** dans **CI/CD** puis dans **Job**. En ce qui me concerne, nous pouvons voir sur la photo ci-dessous que toutes les mises à jour se sont toujours bien effectuées. 
![](../images/vérification.jpg)

Cependant, on peut aussi lancer la mise à jour nous-même. Comment faire? C'est très simple et plutôt rapide (30 secondes). Il faut aller **CI/CD** --> **Schedules**. Ensuite il faut appuyer sur le **Play** pour la lancer. Après il faut aller voir dans **Job** ou **Pipelines** et le tour est joué!
