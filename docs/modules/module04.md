# MODULE 4 : Impressions Laser Cutter

Dans ce module 4, nous avons d'abord suivi une formation sur les imprimantes **Laser** dont nous disposons au Fab Lab : la **Lasersaur** et la **Muse FullSpectrum**. Grâce à cette formation, j'ai appris à utiliser ces imprimantes ainsi que ses réglages pour utiliser ces machines en toute sécurité. Je vais vous expliquer à travers ce module, les différentes étapes à suivre et à savoir pour le bon fonctionnement des machines.

Il nous était également demandé dans ce module, d'**imaginer une lampe** à partir de notre objet initial. Je vais donc également vous expliquer la conceptualisation de ma lampe et les démarches que j'ai entreprises pour l'imprimer avec l'imprimante Laser.

## About the printers

> Lasersaur : machine open-source (celle que l'on va utiliser le plus souvent)

- Très avantageuse en ce qui concerne la qualité de la découpe
- Surface de découpe : 122 cm x 61 cm
- Hauteur maximale : 12 cm
- Vitesse max : 6000 mm/min
- Puissance laser : 100 W
- Type de laser : Tube Co2 (infrarouge)

**Précautions** :

- Connaitre avec certitude le matériau utilisé
- Il faut toujours ouvrir la vanne d’air comprimé
- Savoir où se trouve le bouton d’urgence
- Il faut toujours allumer l’extracteur de fumée

**Interface : DriveboardApp**

- Uniquement les fichiers vectoriels
- SVG : efficace, attention a utiliser des couleurs RVB
- DXF : plus difficile d’exporter un fichier compatible
- Pour importer une image matricielle, il faut l’intégrer dans un fichier SVG

> Muse FullSpectrum

- Surface de découpe (petite machine) : 50 cm x 30 cm
- Hauteur maximale : 6 cm
- Puissance laser : 40 W
- Type de laser : Tube Co2 (infrarouge)
- Pointeur rouge supplémentaire --> permet de se positionner précisément

**Précautions :**

- Connaitre avec certitude le matériau utilisé
- Il faut toujours brancher la pompe à air
- Savoir où se trouvent les boutons pause et stop
- Il faut toujours allumer l’extracteur de fumée

**Interface : Retina Engrave**

- Basé sur navigateur internet ou directement via le portable
- Fichiers vectoriels : SVG
- Fichier sera importé en vectoriel mais aussi en matriciel il faudra donc en supprimer un des deux
- Images matricielles : BMP, JPEG, PNG, TIFF
- Fichiers PDF

## Which materials should be used?

Les bases à savoir pour utiliser les machines sans danger :

- Les matériaux recommandés :

Le bois contreplaqué (3 à 4 mm), l'acrylique car il se découpe très facilement, le papier, le carton et les textiles.

- Les matériaux déconseillés :

Le MDF, ABS, le polystyrène, PE, PET, PP, les composites à base de fibres ainsi que les métaux car ils sont impossible à découper. Ces matériaux produisent trop de fumée et fondent trop facilement.


- Les matériaux interdits :

Le PVC, le cuivre (réfléchit au laser), le téflon (PTFE), le vinyl, le simili-cuir, la résine phénolique et l'époxy. Tous ces matériaux sont nocifs pour la santé des individus.

## How to use the printers?

> Ce qu'il ne faut pas oublier avant de lancer une impression laser :

- Remettre à zéro --> retour à l’origine (bouton Run)
- Vérifier les status (bouton status) —> indicateurs Limit doivent être en vert (pas orange ni rouge)
- Ouvrir un fichier SVG ou DXF (bouton Open)
-—>fonctionne pas sur le navigateur Microsoft Egde

> Deux positionnements :

- Installer le matériau dans la machine
- Déplacer la tête du laser au bon endroit
- Régler la distance focale (hauteur entre lentille et matériau)
- Support suggéré :  15 mm

> Trois réglages :

- Déplacer le dessin si nécessaire (bouton Offset)
- Régler la puissance et la vitesse de travail (bouton +) ainsi que choisir les couleurs
- Régler la vitesse de découpe (F)

**A ne surtout pas oublier :**

- Allumer le refroidissement à eau (bouton noir)
- Allumer l'extracteur de fumée (bouton vert)
- Allumer la panne d’air comprimé
- Remettre le laser sur Home

![](../images/Réglage_imprimante_laser.jpg)

© M.D

Il faut s'assurer que le bouton status soit **vert**, si c'est le cas, on peut appuyer sur **Run** pour lancer l’impression!

## Experimentation

Dans le but d'apprendre à utiliser les machines à découpe Laser, nous avons du, comme expliqué auparavant, conceptualiser une lampe (sur base de notre objet) sur une feuille de polypropylène d'un format 60 cm x 80 cm. Pour ce faire, je me suis d'abord demandé à quoi me faisait penser mon objet et j'en ai ressorti les mots-clés suivant :

- **Totémique**
- Rond
- **Massif**
- Coloré
- **Réfléchissant/brillant**
- Lourd
- Kitsch
- Dur
- Simple
- **Accordéon**

De cette liste, j'ai décidé de choisir les quatre mots qui me parlaient le plus : totémique, réfléchissant/brillant, massif et accordéon. Je voulais également lui conférer une **fonction modulable** tout comme mon plug in. L'idée était de créer une lampe **longue/haute** pour lui donner une silhouette totémique tout comme le tabouret. Il était aussi question de trouver une manière de la faire tourner sur elle-même afin d'obtenir **différentes formes** possibles pour la lampe, donc l'aspect modulable.

Le but de l'exercice était de créer un objet qui n'aura pas besoin de liants pour assembler les différentes pièces. Cette lampe doit s'attacher sous forme d'assemblages ou de pliages. De ce fait, je me suis intéressée aux origamis, art du pliage en papier et plus précisément aux origamis en spirale. Après de nombreuses recherches, je me lancée sur un type d'origami très intéressant pour ma recherche : l'**origami ADN** qui répond à toutes mes idées.  

## Try of origamis

Avant d'imprimer ma lampe à l'aide de l'imprimante Laser, j'ai suivi un [Tutoriel](https://www.youtube.com/watch?v=pB0FMshudqEo) sur cet origami pour parvenir à le représenter en papier. Ce n'est pas forcément difficile mais il ne faut pas se tromper dans les proportions. Si vous voulez de belles spirales il faut une bandelette assez longue, plus elle est longue, plus il y aura de spirales.  

- Premier essai (échec) :
Comme expliqué plus haut, je n'ai pas pris une longueur assez grande, ce qui n'a pas donné le résultat attendu.
![](../images/origami_raté_+_fonctionnement_pliage.jpg)
© M.D  

- Deuxième essai (réussi) :
![](../images/Origami_final.jpg)
© M.D  

## Final idea

Comme je vous l'ai expliqué, mon idée était d'avoir une forme longue dotée d'une silhouette totémique et modulable. Grâce à cet origami, je peux plier ma lampe tout comme la développer entièrement ou partiellement, tout comme un accordéon. Le but de cette lampe est de pouvoir choisir l'intensité de la lumière ou la taille de la lampe tout simplement.

- Prototype de lumière :

![](../images/Lampe_lumière.jpg)
© M.D

## Drawing in 2D

Une fois le prototype réussi, il a fallut dessiner le fonctionnement de l'origami à plat, en 2D. J'ai donc décidé d'utiliser **Autocad**, l'un des logiciels que j'utilise le plus. Je n'ai dû utiliser qu'un seul outil pour réaliser cette forme : l'outil **Ligne** dans **Dessiner**. Afin de savoir différencier les gravures (pour le pliage) et la découpe (pour avoir la forme), j'ai choisi deux couleurs : le bleu pour la gravure et le vert pour la découpe que j'ai choisi dans la catégorie : **Couleur**.

![](../images/Lampe_spirale.jpg)
© M.D

Après avoir fini la modélisation, je crée une présentation sur Autocad pour pouvoir enregistrer mon dessin en **PDF**. Cependant, pour réaliser une impression Laser, il faut avoir le fichier en **SVG**. De ce fait, j'ai utilisé **Illustrator** afin de le convertir en SVG.

## The first printing (fail)

Après les étapes énoncées ci-dessus, je mets mon fichier sur une clé USB pour ensuite entrer la clé sur l'ordinateur du Fablab. Avant de lancer l'impression, il y a plusieurs étapes à entreprendre. Tout d'abord j'ai ouvert mon fichier avec **Inkscape** afin de vérifier que tout est en ordre. Pour que l'impression se déroule correctement, j'ai dégroupé mon objet pour être certaine qu'il n'y avait pas de problème. Ensuite, j'ai positionné l'objet de la manière que je voulais. Pour finir, j'ai enregistré le dossier sur l'ordinateur du FabLab.

Avant d'imprimer, je vais allumer la machine, allumer le refroidissement à eau (bouton noir), allumer l'extracteur de fumé (bouton vert), allumer la panne d’air comprimé pour que l'impression se déroule correctement.

Cependant, avant de lancer l'impression, je vais ouvrir mon fichier sur l'application **Drive BoardApp** déjà installée sur l'ordinateur du FabLab (Open --> ouvrir le fichier). Pour savoir quel couleur doit faire quoi, j'appuie sur le **bouton +** en sélectionnant la couleur bleu pour les gravures que je fais mettre à 15% pour pouvoir effectuer mon pliage une fois imprimé. Après ça, je sélectionne le + d'en dessous pour y mettre la couleur verte que je mets à 30% pour que le laser découpe le polypropylène. J'ai choisi ces pourcentages sous les conseils d'Axel lors de la formation.

La dernière étape est de vérifier que ma feuille de polypropylène est correctement placée sur le laser, j'utilise donc l'outil **Move** pour vérifier cela.

Une fois que tout était en ordre j'ai appuyé sur le bouton **Run** et l'impression s'est lancée. Cette impression à pris 3 petites minutes.

Malheureusement, cette dernière à été un échec, n'ayant fait des gravures que d'un seul coté, le pliage ne fonctionne pas car je me rends compte qu'il doit se plier des deux côtés.

![](../images/Impression_laser_1.jpg)
© M.D

## Drawing in 2D - Version 2

Pour réussir mon impression, je suis alors partie de l'idée de graver la feuille de polypropylène des deux côtés, un défi n'est ce pas? Ce sera un travail de grande précision. Pour ce faire, j'ai créé, grâce à **Autocad**, deux fichiers PDF, l'un avec les gravures sur la face supérieure et un autre avec les gravures inférieures et la découpe. J'ai choisi la couleur mauve pour les gravures supérieures, le bleu pour les gravures inférieures et le vert pour la découpe.

![](../images/Autocad_2.jpg)

![](../images/Impression_Laser_2.jpg)
© M.D

Comme expliqué pour la version 1, je crée une présentation sur Autocad pour pouvoir enregistrer mes dessins en **PDF**. Cependant, pour réaliser une impression Laser, il faut avoir le fichier en **SVG**. De ce fait, j'ai utilisé **Illustrator** afin de le convertir en SVG.

## The second printing (fail again)

Pour lancer cette seconde impression, j'ai effectué exactement les **mêmes opérations** que pour la première. Cependant, j'ai du les faire deux fois et en deux parties. J'ai commencé par lancer la première impression : celle avec les gravures mauves. Une fois celle-ci terminée, j'ai retourné précisément le papier en mesurant tout ce qu'il fallait. Ensuite, j'ai lancé la deuxième impression sur la même feuille : celle avec les gravures bleue et la découpe verte. J'ai utilisé les mêmes pourcentages que pour la première impression : 15% pour les gravures et 30% pour la découpe.

Cependant, cette impression a encore été un échec car je n'ai pas pris en compte l'effet miroir qui allait apparaitre lorsque j'allais retourner la feuille de polypropylène.

![](../images/I.Laser_2.jpg)
© M.D

## Drawing in 2D - Version 3

De ce fait, j'ai de nouveau redessiné l'un des deux fichiers (le bleu et vert) pour parvenir à la bonne combinaison. J'ai tout au long de cette étape, toujours effectué les mêmes étapes et les mêmes opérations que les versions ultérieures.

![](../images/impression_3_laser.jpg)
© M.D

## The third printing (fail again and again)

Pour cette troisième impression, j'ai effectué les mêmes opérations et les mêmes étapes que lors de la deuxième impression. Cette fois-ci la combinaison était la bonne mais le travail de précision était assez compliqué. J'ai du recommencer deux fois l'impression pour tenter d'avoir sur la feuille le pliage correctement reproduit. Cependant, à ce stade, je me rend compte que ca reste assez compliqué de plier la feuille malgré les gravures des deux côtés. Comment réussir??

![](../images/Photo_impression_3.jpg)
© M.D

## The fourth, the fifth and the sixth printing...

Après avoir parlé avec Hélène et Gwen, afin d'avoir plus de chance dans mon **travail de précision**, elles m'ont conseillé d'agrandir l'échelle de ma lampe, ce que j'ai donc fait. Sur **Inkscape**, j'ai ouvert mes deux documents, je les ai placés exactement au même endroit pour ensuite augmenter leur taille. Pour arriver à ce résultat, j'ai sélectionné la forme, actionné le cadenas pour qu'elle ne se déforme pas et j'ai augmenté sa taille par 2.

Après cela, j'ai de nouveau lancé l'impression de la même manière que j'ai lancé les autres. J'ai de nouveau effectué le même travail de précision à l'aide de mesures, d'une équerre et de lattes métalliques. J'ai du réaliser cette impression deux fois avant d'arriver au résultat que je souhaitais.

![](../images/Essai_4.jpg)

![](../images/Essai_5.jpg)
© M.D

## The final result

Après **5h d'acharnement** sur cette impression au FabLab, j'ai finalement réussi à avoir le résultat que je cherchais. J'ai enfin réussi à faire correctement les gravures des deux côtés! C'était éprouvant...

En tentant d'entamer le pliage, je me suis rendu compte que les lignes le long des deux bords de la lampe gênaient le pliage, j'ai donc décidé de les couper afin de réussir à effectuer le pliage.

Le **pliage fonctionne**, c'est une bonne nouvelle! Malheureusement, le matériau n'est **pas assez flexible** pour avoir l'effet que l'on voit sur le prototype en papier. Il se plie, mais se déplie aussitôt!

- Résultat non plié :

![](../images/Final_couché.jpg)

- Résultat plié (du moins une tentative) :

![](../images/résultat_final.jpg)
© M.D

## For the pleasure

Voici une photo de toutes mes tentatives :

![](../images/IMG_6944_2-min.jpg)
© M.D

## Material properties

Pour tenter de réussir à torsader ma lampe jusqu'au bout de la bande et d'obtenir le résultat final, j'ai suivi les conseils de Victor. Je vais essayer de **chauffer le polypropylène** pour le ramollir. Pour se faire, je vais d'abord me renseigner sur les propriétés du matériau en question.

```
> Le polypropylène (PP) :

Faible densité ;
• Très grande dureté ;
• Très bonne résistance à hautes
températures ;
• Grande résistance à la fissuration ;
• Faible absorption d'eau ;
• Grande résistance aux agents chimiques ;
• Thermoformable ;
• Inertie physiologique (certains PP sont "alimentaires).

```
--> Tous ces points proviennent de cette [source](http://sii-technologie.ac-rouen.fr/Microtechniques/BTS_CIM/Moule_Attache_Pin/export/medias/640.pdf).

## Heating the material

Ce qu'il faut savoir sur les températures :
```
-  Thermoformage de la température de transition vitreuse : 160°C à 220°C
```

Il faut savoir que les plastiques sont des **matériaux déformables**, ils peuvent être modulés à chaud et sous pression. Le pliage à droid est quasiment impossible! 

Pour ce faire, j'ai commencé par chauffer mon **four à 70°C**. Ensuite j'y ai placé mon objet et je l'ai laissé pendant quelques minutes. Une fois sorti du four, j'ai tenté de le torsader mais la température n'était pas assez forte. J'ai donc refais la même chose à **80°C** puis à **90°C** puis à **100°C** afin que ça se plie de plus en plus. Cependant, je me rends compte que ça ne donne pas l'effet voulu et que ça me brûle le bout des doigts. 
![](../images/four_photo-min.jpg)
© M.D

L'idéal dans mon cas est d'avoir une **thermoplieuse**, elle va permettre de plier facilement le matériau en le chauffant à la bonne température. Comment fonctionne-t-elle? l'explication se trouve [ici](https://www.youtube.com/watch?v=H701w3nCgMU) à l'aide d'une vidéo. Malheureusement, je n'ai pas de thermoplieuse ce qui ne me permet pas d'effectuer mon pliage jusqu'au bout. Je cherche toujours une solution. 
