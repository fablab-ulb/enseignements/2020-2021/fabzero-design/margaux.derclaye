# MODULE 3 : Impression 3D - PrusaSlicer I3 MK3S

Dans ce module 3, j'ai été formée pour pouvoir utiliser le programme **PrusaSlicer** afin de parvenir à lancer une impression 3D et d'utiliser une imprimante 3D correctement. Le FabLab dispose de six **imprimantes Prusa I3 MK3S** que nous pouvons utiliser autant que nous voulons sur rendez-vous. Dans cette partie, je vais donc vous expliquer tout mon processus d'impression pour mes deux pièces : le tabouret d'origine et le plug in.

**À savoir :** Comme Gwen et Hélène nous l'ont expliqué, ces imprimantes sont assez pratiques car si l'une de ces pièces est cassée, une autre imprimante peut réimprimer la pièce manquante afin de réparer l'autre.

## Program Prusa

Pour pouvoir utiliser le programme, j'ai tout d'abord du télécharger le logiciel Prusa3D sur mon ordinateur en allant sur le [site](https://www.prusa3d.com/drivers/) de Prusa en choisissant la version _DRIVERS & APPS 2.2.9.1_ pour MacOS. Ce programme s'est téléchargé très facilement et sans aucun problème.

Pour utiliser ce programme, j'ai tout d'abord exporté et enregistré sur mon bureau mes deux fichiers Fusion 360 en **SLT**. Après les avoir exportés, j'ai importé chacun à leur tour, les fichiers SLT dans le logiciel Prusa. En les important, je me suis rendue compte que mes objets étaient trop grands, j'ai alors directement réduit leurs tailles dans le programme à un facteur de redimensionnement de 35% (x= 61.23 y=61.24 z=77).

Pour le tabouret existant, j'ai disposé l'objet verticalement au centre de la plateforme afin d'éviter le plus possible d'avoir des supports/renforts et qu'il soit disposé de manière la plus solide pour qu'il ne bouge pas. Malgré le fait qu'il soit dans la meilleure position possible, mon objet aura de toute façon besoin de support pour qu'il ne bascule pas pendant l'impression en raison de sa hauteur.

Pour le plug in, afin de ne pas monopoliser toutes les imprimantes, j'ai importé les 6 fichiers SLT sur la même plateforme. Tout comme le tabouret d'origine, j'ai positionné les objets verticalement pour une meilleure solidité. Celui-ci n'aura pas besoin de renforts car toutes les pièces sont séparées, elles ne vont donc pas se renverser lors de l'impression.

## Settings

Lors de la formation, nous avons eu beaucoup d'informations afin d'obtenir un bon parametrage sur le logiciel PrusaSlicer. Il est important de d'abord mettre son logiciel en mode **Expert**. Cette fonction nous permet d'avoir plus de paramètres afin d'avoir plus de précision lors de l'impression de l'objet.

Avant de lancer mes impressions, je vais vous expliquer les différents réglages que j'ai effectués et pourquoi j'ai fait ces réglages :

> Couches et périmètres :

1. Hauteur de la première couche : 0.2 mm
2. Parois verticales : pèrimètre : 3 (afin d'avoir une meilleure solidité de l'objet après l'impression)

--> Voici les deux paramètres que je vais changer dans cette partie, tout le reste ne bouge pas.

![](../images/réglages_1.jpg)

> Remplissage :

1. Densité de remplissage : 10%
2. Motif de remplissage (pour le modèle existant) : gyroïde
3. Motif de remplissage (pour le nouveau modèle) : rectiligne (afin que l'impression prenne moins de temps)

--> Voici les deux paramètres que je vais changer dans cette partie, tout le reste ne bouge pas.

![](../images/réglage_2.jpg)

> Jupe et bordure :

1. Bordure : largeur de la bordure : 3

Normalement, c'est un réglage par défaut de 1, mais comme mon objet est assez haut, j'ai décidé de le mettre sur 3 pour m'assurer que celui-ci resterait stable sur le plateau.

--> Voici le seul paramètre que je vais changer dans cette partie, tout le reste ne bouge pas.

![](../images/réglage_3.jpg)

> Supports :

1. Générer des supports : oui
2. Ne pas supporter les ponts : oui

J'ai généré des supports supplémentaires (pour le tabouret d'origine) pour maintenir certaines parties de mon objet afin qu'il ne tombe pas lors de l'impression. Si il n'a pas besoin de supports supplémentaires, on les génère uniquement sur le plateau. Pour le plug in, je n'en ai pas rajouté car il n'en n'avait pas besoin, il était suffisamment solide.  

__
--> Voici les deux paramètres que je vais changer dans cette partie, tout le reste ne bouge pas.

![](../images/réglage_4.jpg)

## Cutting view

Une fois tous les réglages effectués, on revient sur le plateau et on regarde si tout va bien en appuyant sur **Découper maintenant**. Si c'est le cas alors on peut exporter le fichier en G-code. J'ai donc exporté mes deux fichiers en **G-Code**.

![](../images/decoupe.jpg)
© M.D   

## Start printing

Après avoir exporté le G-code, j'ai mis ce fichier sur une **carte SD** dans le fichier Margaux pour ensuite l'insérer dans l'imprimante 3D. Une fois la carte SD insérée dans la machine, j'ai ouvert le fichier et sélectionné mon document. L'impression a donc commencé après avoir atteint la bonne température de **250°**. Il ne faut surtout pas oublier de rester à côté de notre impression pendant les trois premières couches afin de s'assurer que celle-ci se lance correctement. Il ne faut également pas augmenter la vitesse de **100%** au début de l'impression, si l'objet le permet on peut le faire par la suite.

## First printing (success)

- Tabouret d'origine :

Comme vous pouvez le voir, cette impression a été réalisée avec le fil noir et a été une réussite dès la première impression! Seuls les supports ont été difficiles à retirer. J'ai réussi à tous les enlever après 40 minutes et à l'aide d'une pince.

L'impression a duré 4h30. Je suppose que c'est dû à son aspect massif.

![](../images/module.base.photo.jpg)
© M.D   

## Second printing (fail..)

- Plug in :

Cette seconde impression a été un échec, non pas dans sa réalisation lors de l'impression mais simplement car je me suis trompée dans les mesures du tube. Je l'ai fait exactement de la même taille que les anneaux, ce qui m'a empêché d'assembler les différentes parties ensemble.

Cette impression a prit 6h45.
![](../images/3D_echec.jpg)

## Third printing (success)

- Plug in :

Pour cette impression, j'ai donc du reprendre le tube sur le logiciel Fusion 360 en le diminuant de 2 mm (30 mm --> 28 mm) afin que les anneaux puissent passer dans le tube. Une fois le tube modifié, j'ai refait les étapes expliquées précédemment : exporté le fichier en SLT --> PrusaSlicer --> Réglages --> Découper --> Exporté en G-code --> Carte SD --> Lancer l'impression.

Cette dernière à été effectuée en fil bleu (plus joli d'ailleurs) et a duré 2h car j'ai seulement du réimprimer l'anneau + le tube puisque l'objet à été réalisé en 6 pièces distinctes.

- Pièces séparées :
![](../images/6_pièces.jpg)

- Pièces assemblées :
![](../images/new.module.jpg)
© M.D  

N.B. : le personnage n'est pas a l'échelle, c'est purement esthétique.
