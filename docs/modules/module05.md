# MODULE 5 : The Shaper Machine

Dans ce module 5, nous avons eu la chance de bénéficier d'une dernière formation : la **Shaper**. Cette machine est une petite machine-outil qui permet de réaliser des découpes ou des gravures à l'aide de différentes types de fraises sur un matériau de la taille que l'on souhaite, il n'y a pas de limites dans ce cas-ci. Si tu veux décrouvir par toi même comment cette machine fonctionne et quelles sont les précautions à connaitre pour l'utiliser, tu peux suivre les démarches sur son [Site Officiel](https://www.shapertools.com/fr-fr/origin/spec) ou continuer à suivre mes explications. 

# The machine

Cette machine est assez simple à utiliser mais pour être certain d'y parvenir correctement il faut bien connaitre la machine et savoir où ses différents boutons se trouvent et quelles sont ses limites. Il est également important de savoir de quoi sa boîte se compose.

**Eléments de la machine**

- _Écran tactile_: multi-touch réactif LCD couleur

- _Port d'extraction de poussière_ : conçu pour la broche SM-1 --> tuyaux d'aspiration de 27 mm à 36 mm

- _WiFi & USB_ : connectivité, mises à jour des logiciels en ligne, transfert des fichiers de conception via ShaperHub et le port USB vont permettre de transfèrer les fichiers facilement 

- _Puissance d'entrée_ : 230 VAC (50HZ)

- _Broche moteur à balais_ : 720 Watt (AC), 10.000 - 26.000 RPM avec contrôle de vitesse à compensation de charge mais avec également des dispositifs de sécurité en cas de démarrage progressif, de surchauffe et de surcharge

- _Collet 8 mm_ : livré installé --> accepte tous les fraises de diamètre de queue de 8 mm

- _Profondeur maximum de l'axe Z_ : 43 mm / 1.7 po --> profondeur de fraisage réglable numériquement avec un capteur tactile automatique

![](../images/machine_shaper-min.jpg)

![](../images/shaper_2ok-min.jpg)

**Inclus avec la machine (nécessaire)**

![](../images/accesoires-min.jpg)

1. **Systainer Tanos** (393.7 mm L x 292.1 mm P x 431.8 mm H)

2. **Shaper Tape** --> 2 Rouleaux (45 mètres chacun)

3. **Clé T-Hexagonale** 4 mm (utile pour le retrait de la broche)

4. **Fraises** de 3 styles différents : fraise hélicoïdale (6 mm), fraise hélicoïdale (3 mm), fraise à graver (angle de pointe 60°)

5. **Clé** de 16 mm (utile pour le Collet)

6. **Tuyau adaptateur** pour les aspirateurs eau/sèche

# Things to know about the machine 

Avant de se lancer dans un quelconque travail de gravures ou de découpes, il faut connaître ses limites, il y a donc trois petites choses à savoir et à retenir : 

- _Ø du collet_ : 8 mm ou 1/8"

- _Profondeur de découpe (max)_: 43 mm

- _Format de fichier_ : SVG

# Before starting

Avant de commencer à utiliser la Shaper, il est très important de faire attention à ce que rien ne soit prit dans la machine lors de son utilisation. Il est nécessaire de bien s'attacher les cheveux (si ils sont longs), de ne pas porter de bijoux longs et avoir des vêtements qui se portent près du corps et non amples. Le manuel d'utilisation conseille également de mettre les lunettes de sécurité pour éviter d'avoir des résidus de bois (ou autres) dans les yeux ainsi que mettre un casque pour limiter le bruit de la machine. Normalement, si il y a un aspirateur branché à la machine, les lunettes ne sont pas nécessaire car on ne risque pas d'avoir des rédisus dans les yeux. La machine étant peu bruyante, le casque est rarement utile. 

# Installation of accessories : précautions 

- Fixation du matériau utilisé 

Avant de commencer quoi que ce soit, il est important d'avoir un espace de travail dégagé, très stable et lisse pour éviter tout risque. Après avoir choisi son plan de travail, il faut fixer sa planche sur le plan de travail à l'aide de de scotch double face sur toutes les parties de la planche pour ne pas que les morceaux découpés qui s'enlèvent et ruine la découpe. Il faut placer le scotch de manière judicieuse en fonction de la forme que l'on veut réalisé. Si on est pas sur que le double face tienne correctement le matériau en question, on peut également viser la planche sur un plan de travail adapté pour plus de solidité lors de la découpe. Il est également important d'utliser la machine au centre du plan de travail pour éviter de la faire tomber. 

![](../images/shaper_photo1-min-min.jpg)

- Délimitation du périmètre de découpe 

Pour que la machine détecte la planche sur laquelle elle doit travailler, il faut placer du scotch particulier (comme sur la photo) sur la planche pour qu'elle puisse scanner la partie voulue avec sa caméra pour détecter les dimensions de la planche. Pour que la caméra détecte ses papiers, il faut les placer à des endroits stratégiques et en utiliser le moins possible (au moins 4) pour ne pas en gaspiller. 

# How to use the Shaper? 

Pour être certain de bien savoir l'utiliser, je vous renvoie au début de ce module pour se remémorer les différents boutons qu'on aura besoin d'utiliser. 

Avant toute chose, il faudra s'occuper de la **gestion de la fraise** : il existe deux boutons pour cette partie, un à gauche et un à droite :
- Bouton vert (droite) : va permettre de lancer la fraise et la descendre jusqu'au matériau 
- Bouton orange (gauche) : va permettre de la faire remonter 

Il faut également savoir que la fraise est très fragile, il faut impérativement la relever avant d'enlever la machine du matériau car non seulement ça risque de la casser mais aussi d'abîmer la découpe/le matériau. 

On peut également changer la fraise, pour ce faire, il faut utiliser un _collet_ pour la desserer et pour ensuite la resserer. Avant ça, il faut que la machine soit éteinte pour faire le changement mais aussi débrancher la fraiseuse du reste de la machine. 

Ensuite, il faut savoir gérer les différentes vitesses de fraisage et connaitre deux choses :

- Si la vitesse est trop basse, la ligne de découpe ne sera pas propre et il risque d'avoir des acouts.
- Par contre, si elle est trop élevée, ça risque de brûler le matériau ou la fraise risque de surchauffer et se casser. 

Avant d'utiliser la machine, n'hésitez pas à faire des tests pour savoir quelle vitesse est la plus adaptée.

**A savoir** : 

- N'hésitez pas à passer plusieurs fois sur le même trait pour avoir une découpe parfaite. 

![](../images/shaper_photo_2-min-min.jpg)

# Stages of the machine

1. Création du périmètre de travail : endroit stable et lisse

2. Connecter la machine à l'aspirateur (pour éviter les résidus)

3. Brancher la machine dans une prise

4. Scanner son périmètre de travail 

5. Importer notre fichier SVG grâce à une clef USB

6. Lancer "fraiser" pour ensuite effectuer les réglages souhaités

Réglages : 

- Choix du type de découpe 
- Emlacement de la découpe 

7. Appuyer sur Z TOUCH pour voir la profondeur du matériau 

8. Allumer le fraisage grâce au bouton vert + suivre le dessin projetté sur la machine, faire les mêmes mouvements et le tour est joué!

![](../images/shaper_3-min-min.jpg)
