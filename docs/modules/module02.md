# MODULE 2 : Logiciel Fusion 360

Dans ce module 2, j'ai appris à utiliser le logiciel de conception **Fusion 360**, évoqué dans le module 1. Ce logiciel m'a permis de modéliser mon objet -Tabouret Pilastro- à l'échelle 1/2 afin de pouvoir le comprendre dans sa globalité : sa forme et sa matérialité.

## Research

Avant de commencer à modéliser mon objet sur Fusion 360, j'ai débuté par des recherches sur Internet afin d'avoir toutes les informations nécessaires à la bonne compréhension du Tabouret Pilastro et de pouvoir me les approprier.

- [Pilastro - Kartell](http://www.kartell-boutique.fr/fr/catalogue/tabouret-et-chaise-haute/produit-pilastro)
- [Tabouret Kartell Design](https://www.sediarreda.com/fr/stores/kartell/tabourets)
- [PILASTRO](https://www.kartell.com/GR/fr/collection-par-défaut-pour-les-produits/pilastro/08852)

## Dimensions

Étant un objet très connu dans le monde du Design, les dimensions me sont facilement parvenues. De plus, beaucoup de modélisations 2D et 3D de cet objet existaient déjà sur des sites Internet, ce qui m'a permis d'avoir toutes les mesures suivantes. Ce qui est indispensable pour comprendre l'objet étudié.

1. Profondeur : 35 cm
2. Largeur : 35 cm
3. Hauteur d'assisse : 46 cm
4. Hauteur du tube : 8,5 cm
5. Hauteur du rond : 7 cm
6. Poids : 3,53 kg
![](../images/dimensions.jpg)
© M.D   

## Materiality

Cet objet a été conçu en **technopolymère thermoplastique teinté dans la masse**. Ce tabouret est fait de matériaux composites, renforcé de fibre de verre (souvent utilisée dans les applications industrielles) et mélangé avec du thermoplastique. Le thermoplastique se ramollit lorsqu'il est suffisamment chauffé pour ensuite, une fois refroidi, devenir dur. Ce procédé a permis de conférer cette forme totémique et originale/kitsch. Après avoir obtenu la forme, elle a été teintée avec de la peinture légèrement brillante couleur lilas, vert, noir, rouge ou violet.

## Form

Afin de comprendre sa forme, j'ai décidé de redessiner l'objet et d'ensuite le déconstruire sur **Autocad** pour comprendre comment il avait été conçu/imaginé. Cette oeuvre se compose de **5 parties** : _3 anneaux/lignes_ dont un qui sert pour l'assise et _2 tubes_ entres ces anneaux/lignes.
![](../images/autocad.jpg)
© M.D   

Cette forme, tout comme son nom l'indique, me fait penser à un **Pilastre** _de type ionique grec_, un support rectangulaire ou carré qui se compose d'une base ou d'un chapiteau. Il ne sert généralement pas comme mur porteur mais plutôt d'élément décoratif, tout comme le tabouret. Cependant, le tabouret ressemble plus à une colonne qu'à un pilastre, ce qui est normal étant donné qu'ils ne sont pas fort différents, ils sont plutôt assez similaires : ils ont les mêmes proportions, les mêmes éléments, la même fonction et la même composition.
![](../images/Pilastre.jpg)
© Internet

Cette forme me fait également penser aux poteries tiébélé en terre cuite, elles sont authentiques. Elles évoquent la nature, la terre, la féminité et le temps. Une fois empilées les unes sur les autres, elles se rapprochent encore plus de mon tabouret.  
![](../images/Tiébélé.jpg)
© Internet

## Object modeling

> Modélisation du tabouret d'origine :

Pour dessiner cet objet sur le logiciel Fusion 360, je me suis d'abord demandé comment j'allais y parvenir. Je me suis demandé si j'allais d'abord le dessiner en 2D ou si j'allais le dessiner directement en 3D puisque ce programme propose les deux options. Étant un objet de forme assez simple, je me suis lancée dans sa modélisation directement en 3D. Je n'ai pas dessiné l'objet à sa taille réelle mais j'ai divisé toutes ses dimensions par 2 pour l'avoir en taille plus réduite. Tout comme le Designer, Ettore Sottsass, j'ai modélisé l'objet en 6 parties :

**1.** Pour commencer, j'ai choisi la **Catégorie Solide** afin de pouvoir le dessiner directement en 3D. Par la suite, j'ai choisi l'outil **Cylindre** pour dessiner l'anneau du bas en choisissant 40 mm pour la hauteur et 175 mm de diamètre.

**2.** Pour cette seconde manipulation, j'ai utilisé les mêmes outils que précédemment afin de dessiner le tube du tabouret mais en changeant les mesures : 425 mm de hauteur et 125 de diamètre.

**3.** Pour la troisième manipulation, j'ai effectué la même manipulation que la première.

**4.** Pour la quatrième manipulation, j'ai effectué la même manipulation que la deuxième.

**5.** Pour la troisième manipulation, j'ai effectué la même manipulation que la troisième.

**6.** Pour finir et afin de reproduire l'objet de manière identique, j'ai utilisé l'outil **Congé** afin de courber les trois anneaux du tabouret sur ses extrémités.

![](../images/3D.base.jpg)

## Plug in

> Explication du nouveau Design (Plug in) :

Une fois l'objet existant dessiné sur le programme, j'ai réfléchi à une autre manière de conceptualiser l'objet et de lui rajouter une nouvelle fonctionnalité : utiliser cet objet également comme étagère en plus d'être un tabouret et une table d'appoint. J'ai décidé dans cette nouvelle version, de garder uniquement les anneaux/lignes et de remplacer les tubes par des anneaux supplémentaires. Tout ces anneaux sont reliés entre eux par un long tube et peuvent tourner autour de celui-ci. L'utilisateur peut alors disposer les anneaux comme il le souhaite. Il peut soit aligner les anneaux et s'assoir dessus ou l'utiliser comme table, soit disposer les anneaux de la manière voulue et l'utiliser comme étagère.

Afin de  pouvoir faire pivoter ces anneaux autour du tube après l'impression, j'ai du dessiner l'objet en trois parties : la base + le tube, les anneaux qui vont pivoter et le "capuchon" qui va refermer l'assemblage.

**1.** Pour effectuer la première pièce, j'ai choisi l'outil **Cylindre** pour dessiner l'anneau du bas en choisissant 40 mm pour la hauteur et 175 mm de diamètre. Ensuite, j'ai choisi l'outil **En Surface** puis l'outil **Cercle** pour dessiner en 2D un cercle de 28 mm de diamètre pour créer le tube avec une hauteur de 190 mm en plus de la base.

**2.** Pour la deuxième pièce, j'ai choisi l'outil **Cylindre** pour dessiner l'anneau du bas en choisissant 40 mm pour la hauteur et 175 mm de diamètre. Ensuite, j'ai choisi l'outil **En Surface** puis l'outil **Cercle** pour dessiner en 2D un cercle de 28 mm de diamètre pour créer le trou en utilisant l'objet **Extrusion**. Cette pièce va devoir être imprimée 4 fois.

**3.** Pour la troisième pièce, j'ai choisi l'outil **Cylindre** pour dessiner l'anneau du bas en choisissant 40 mm pour la hauteur et 175 mm de diamètre. Ensuite, j'ai choisi l'outil **En Surface** puis l'outil **Cercle** pour dessiner en 2D un cercle de 28 mm de diamètre pour créer un renforcement dans le "capuchon" en utilisant l'objet **Extrusion**.

**4.** Une fois toutes ces pièces assemblées, voici le résultat attendu après l'impression :


![](../images/Fusion_plug_in.jpg)
